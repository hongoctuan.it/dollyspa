-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 16, 2021 at 02:32 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dolly`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `slogan` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `slogan`, `short_des`, `detail_des`, `slug`, `img`) VALUES
(1, '1', 'Thành lập từ năm 2010, Cát Mộc Spa ẩn mình tại Quận 1, Tp. HCM và mang hơi thở của kiến trúc Việt Nam đương đại kết hợp hài hòa với thiên nhiên và hương thơm tinh dầu... [Xem thêm]', '323', 'gioi_thieu', '1557255425_123.png');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `customer_id`, `date`, `time`, `number`, `product`, `note`, `active`) VALUES
(12, 28, '1623258000', '12', 2, 76, 'khong có note', 1),
(13, 29, '1623430800', '12', 2, 37, '2', 1),
(14, 30, '1623344400', '12', 1, 37, '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `level` int(11) NOT NULL,
  `img_large` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `img`, `parent`, `slug`, `active`, `deleted`, `level`, `img_large`) VALUES
(1, 'Chăm sóc da', '', NULL, 0, 'cham-soc-da', 1, 0, 0, '1558095411_img_content_header_home_use.jpg'),
(2, 'Sức khoẻ', NULL, NULL, 0, 'suc-khoe', 1, 0, 0, ''),
(3, 'Chăm sóc cơ thể', 'mo ta ngan', NULL, 0, 'cham-soc-co-the', 1, 0, 0, ''),
(4, 'Gói dịch vụ', NULL, NULL, 0, 'goi-dich-vu', 1, 0, 0, ''),
(17, 'làm đẹp 2', '', NULL, 0, 'lam-dep-2', 1, 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_updated` datetime NOT NULL,
  `previous_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `info`, `value`, `last_updated`, `previous_value`) VALUES
(1, 'address', '45b Đường Dân Tộc, Phường Tân Thạnh, Quận Tân Phú', '2019-05-18 14:15:08', NULL),
(2, 'phone', '093 144 47 60', '2019-05-18 14:15:08', NULL),
(3, 'email', 'hongoctuan.it@gmail.com', '2021-05-22 04:12:51', NULL),
(4, 'intro', 'Cùng với đội ngũ nhân viên chuyên nghiệp và chu đáo, Dolly Spa trở thành địa điểm uy tín của các anh chị văn phòng chăm sóc, trị liệu da mặt định kỳ với dòng mỹ phẩm chuyên nghiệp phổ thông của Hoa Kỳ, Pháp và thiên nhiên được chọn lọc để mang đến một kế', '2021-05-22 04:12:51', NULL),
(5, 'time', '<ul>\r\n	<li>9.30 - 22.00</li>\r\n	<li>Lễ - Tết:Mở cửa</li>\r\n	<li>Tết &Acirc;m lịch:Đ&oacute;ng cửa</li>\r\n	<li>Chốt hẹn:8.30 pm</li>\r\n</ul>\r\n', '2021-05-22 04:13:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `phone`, `create_date`, `type`, `active`) VALUES
(18, 'thoa', '0377115001', '1622036148', 0, 1),
(19, 'tuan', '0347366361', '', 0, 1),
(26, 'tuanhn 3', '0347366363', '1623389301', 1, 1),
(27, 'tuanhn 4', '0347366364', '1623389324', 1, 1),
(28, 'tuanhn 5', '0347366365', '1623402421', 1, 1),
(29, 'tuanhn 6', '0347366366', '1623467153', 1, 1),
(30, 'tuanhn 7', '0347366367', '1623467440', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `detaildistribution`
--

CREATE TABLE `detaildistribution` (
  `id` int(11) NOT NULL,
  `distributionId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL,
  `paymentProduct` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distribution`
--

CREATE TABLE `distribution` (
  `id` int(11) NOT NULL,
  `distributorId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `totalPayment` int(11) NOT NULL DEFAULT 0,
  `userId` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE `distributor` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_detail`
--

CREATE TABLE `gallery_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` text COLLATE utf8_unicode_ci NOT NULL,
  `end_date` int(11) NOT NULL,
  `discount` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallery_detail`
--

INSERT INTO `gallery_detail` (`id`, `name`, `description`, `img`, `gallery_id`, `active`, `slug`, `start_date`, `end_date`, `discount`, `deleted`) VALUES
(33, 'Tặng 30% khách hàng thân thiết', '<div class=\"clearfix text_block\">\r\n<p><strong><span style=\"color:#ff6900\">Tặng 30%</span></strong><br />\r\n<span style=\"color:#313131\">thẻ mệnh gi&aacute; 3.000.000đ, hay<br />\r\nNh&oacute;m bạn đi c&ugrave;ng 4 người </span><strong><span style=\"color:#313131\">*</span>*</strong></p>\r\n\r\n<p style=\"font-size:14px\"><span style=\"color:#313131\">&Aacute;p dụng tất cả c&aacute;c ng&agrave;y trong tuần, kh&ocirc;ng &aacute;p dụng cuối tuần<br />\r\nKh&ocirc;ng &aacute;p dụng cho c&aacute;c dịch vụ lẻ như gội đầu, x&ocirc;ng hơi, mặt nạ<br />\r\nThẻ sử dụng tối đa 2 người/ng&agrave;y v&agrave; c&oacute; gi&aacute; trị 6 th&aacute;ng từ ng&agrave;y ph&aacute;t h&agrave;nh<br />\r\n<strong>**</strong> Chương tr&igrave;nh khuy&ecirc;n m&atilde;i d&agrave;nh cho nh&oacute;m chỉ &aacute;p dụng cho dịch vụ v&agrave; c&oacute; gi&aacute; trị đến 30/12/2020.</span></p>\r\n</div>\r\n', '1621953123_tin_tuc_01.jpeg', 0, 1, 'Tặng_30_khách_hàng_thân_thiết', '1622566800', 1627146000, '30', 0),
(34, 'Tặng 20% cho khách hàng mới', '<div class=\"clearfix text_block\">\r\n<p><span style=\"color:#ff6900\"><strong><strong>Tặng 20%</strong></strong></span><br />\r\n<span style=\"color:#313131\">thẻ mệnh gi&aacute; 2.000.000đ</span></p>\r\n\r\n<p style=\"font-size:14px\"><span style=\"color:#313131\">&Aacute;p dụng tất cả c&aacute;c ng&agrave;y trong tuần, kh&ocirc;ng &aacute;p dụng cuối tuần<br />\r\nKh&ocirc;ng &aacute;p dụng cho c&aacute;c dịch vụ lẻ như gội đầu, x&ocirc;ng hơi, mặt nạ<br />\r\nThẻ sử dụng tối đa 2 người/ng&agrave;y v&agrave; c&oacute; gi&aacute; trị 6 th&aacute;ng từ ng&agrave;y ph&aacute;t h&agrave;nh<br />\r\n<strong>**</strong> Chương tr&igrave;nh khuy&ecirc;n m&atilde;i d&agrave;nh cho nh&oacute;m chỉ &aacute;p dụng cho dịch vụ v&agrave; c&oacute; gi&aacute; trị đến 30/12/2020.</span></p>\r\n</div>\r\n', '1621953193_tin_tuc_02.jpeg', 0, 1, 'Tặng_20_cho_khách_hàng_mới', '1622480400', 1625072400, '20', 0),
(35, 'Giảm giá 10% cho 2 người', '<div class=\"clearfix text_block\">\r\n<p><strong><span style=\"color:#ff6900\">Tặng 10%</span></strong></p>\r\n\r\n<p><span style=\"color:#313131\">&Aacute;p dụng tất cả c&aacute;c ng&agrave;y trong tuần, kh&ocirc;ng &aacute;p dụng cuối tuần<br />\r\nKh&ocirc;ng &aacute;p dụng cho c&aacute;c dịch vụ lẻ như gội đầu, x&ocirc;ng hơi, mặt nạ<br />\r\nThẻ sử dụng tối đa 2 người/ng&agrave;y v&agrave; c&oacute; gi&aacute; trị 6 th&aacute;ng từ ng&agrave;y ph&aacute;t h&agrave;nh<br />\r\n<strong>**</strong> Chương tr&igrave;nh khuy&ecirc;n m&atilde;i d&agrave;nh cho nh&oacute;m chỉ &aacute;p dụng cho dịch vụ v&agrave; c&oacute; gi&aacute; trị đến 30/12/2020.</span></p>\r\n</div>\r\n', '1621953238_tin_tuc-04.jpeg', 0, 1, 'Giảm_giá_10_cho_2_người', '1620666000', 1624986000, '10', 0),
(36, 'Giảm giá 15% cho khác hàng mua combo bất kì', '<div class=\"clearfix text_block\">\r\n<p><strong><span style=\"color:#ff6900\">Tặng 15%</span></strong><br />\r\n<span style=\"color:#313131\">thẻ mệnh gi&aacute; 3.000.000đ</span><br />\r\n<span style=\"color:#313131\">&Aacute;p dụng tất cả c&aacute;c ng&agrave;y trong tuần, kh&ocirc;ng &aacute;p dụng cuối tuần<br />\r\nKh&ocirc;ng &aacute;p dụng cho c&aacute;c dịch vụ lẻ như gội đầu, x&ocirc;ng hơi, mặt nạ<br />\r\nThẻ sử dụng tối đa 2 người/ng&agrave;y v&agrave; c&oacute; gi&aacute; trị 6 th&aacute;ng từ ng&agrave;y ph&aacute;t h&agrave;nh<br />\r\n<strong>**</strong> Chương tr&igrave;nh khuy&ecirc;n m&atilde;i d&agrave;nh cho nh&oacute;m chỉ &aacute;p dụng cho dịch vụ v&agrave; c&oacute; gi&aacute; trị đến 30/12/2020.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n', '1621953313_tin_tuc_02.jpeg', 0, 1, 'Giảm_giá_15_cho_khác_hàng_mua_combo_bất_kì', '1623085200', 1625072400, '15', 0),
(37, 'Tặng thêm 1 lần massage body', '<div class=\"clearfix text_block\">\r\n<p><strong><span style=\"color:#ff6900\">Tặng Th&ecirc;m 1 lần massage body</span></strong></p>\r\n\r\n<p style=\"font-size:14px\"><span style=\"color:#313131\">&Aacute;p dụng tất cả c&aacute;c ng&agrave;y trong tuần, kh&ocirc;ng &aacute;p dụng cuối tuần<br />\r\nKh&ocirc;ng &aacute;p dụng cho c&aacute;c dịch vụ lẻ như gội đầu, x&ocirc;ng hơi, mặt nạ<br />\r\nThẻ sử dụng tối đa 2 người/ng&agrave;y v&agrave; c&oacute; gi&aacute; trị 6 th&aacute;ng từ ng&agrave;y ph&aacute;t h&agrave;nh<br />\r\n<strong>**</strong> Chương tr&igrave;nh khuy&ecirc;n m&atilde;i d&agrave;nh cho nh&oacute;m chỉ &aacute;p dụng cho dịch vụ v&agrave; c&oacute; gi&aacute; trị đến 30/12/2020.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n', '1621953364_login_background.jpeg', 0, 1, 'Tặng_thêm_1_lần_massage_body', '1622480400', 1625072400, '0', 0),
(38, 'Tưng Bừng Ưu Đãi Sinh Nhật Spa 10 năm', '<div class=\"clearfix text_block\">\r\n<p><strong><span style=\"color:#ff6900\">Tặng 30%</span></strong><br />\r\n<span style=\"color:#313131\">thẻ mệnh gi&aacute; 3.000.000đ, hay<br />\r\nNh&oacute;m bạn đi c&ugrave;ng 4 người </span><strong><span style=\"color:#313131\">*</span>*</strong></p>\r\n\r\n<p><span style=\"color:#ff6900\"><strong><strong>Tặng 20%</strong></strong></span><br />\r\n<span style=\"color:#313131\">thẻ mệnh gi&aacute; 2.000.000đ</span></p>\r\n\r\n<p style=\"font-size:14px\"><span style=\"color:#313131\">&Aacute;p dụng tất cả c&aacute;c ng&agrave;y trong tuần, kh&ocirc;ng &aacute;p dụng cuối tuần<br />\r\nKh&ocirc;ng &aacute;p dụng cho c&aacute;c dịch vụ lẻ như gội đầu, x&ocirc;ng hơi, mặt nạ<br />\r\nThẻ sử dụng tối đa 2 người/ng&agrave;y v&agrave; c&oacute; gi&aacute; trị 6 th&aacute;ng từ ng&agrave;y ph&aacute;t h&agrave;nh<br />\r\n<strong>**</strong> Chương tr&igrave;nh khuy&ecirc;n m&atilde;i d&agrave;nh cho nh&oacute;m chỉ &aacute;p dụng cho dịch vụ v&agrave; c&oacute; gi&aacute; trị đến 30/12/2020.</span></p>\r\n</div>\r\n', '1621953062_promotion.jpg', 0, 1, 'Tưng_Bừng_Ưu_Đãi_Sinh_Nhật_Spa_10_năm', '1622480400', 1625072400, '10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `time` text COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `title`, `time`, `des`, `img`, `deleted`) VALUES
(1, '15', '23', '34', '1557184038_123.png', 0),
(2, '1', '2', '2', '1557184176_screen_shot_2019-03-24_at_23.01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

CREATE TABLE `home_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) DEFAULT 1,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_banner`
--

INSERT INTO `home_banner` (`id`, `name`, `promotion_id`, `img`, `active`, `deleted`, `link`, `description`) VALUES
(1, 'Banner 1', 1, '1556574471_home-banner-slide.jpg', 1, 0, NULL, NULL),
(4, ' ', NULL, '', 1, 1, ' ', ''),
(5, '', NULL, '1621866102_login_background.jpeg', 1, 1, NULL, NULL),
(6, '', NULL, '1621866116_login_background.jpeg', 1, 1, NULL, NULL),
(7, '', NULL, '1623350683_banner_01.jpeg', 1, 0, NULL, NULL),
(8, '', NULL, '1623350896_banner_02.jpeg', 1, 0, NULL, NULL),
(9, '', NULL, '1623351006_banner_03.jpeg', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_hot_product`
--

CREATE TABLE `home_hot_product` (
  `id` int(11) NOT NULL,
  `img` int(11) NOT NULL,
  `des_full` text COLLATE utf8_unicode_ci NOT NULL,
  `des_01` text COLLATE utf8_unicode_ci NOT NULL,
  `des_02` text COLLATE utf8_unicode_ci NOT NULL,
  `des_03` text COLLATE utf8_unicode_ci NOT NULL,
  `des_04` text COLLATE utf8_unicode_ci NOT NULL,
  `des_05` text COLLATE utf8_unicode_ci NOT NULL,
  `des_06` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_hot_product`
--

INSERT INTO `home_hot_product` (`id`, `img`, `des_full`, `des_01`, `des_02`, `des_03`, `des_04`, `des_05`, `des_06`) VALUES
(1, 1558446179, '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `home_info_compa`
--

CREATE TABLE `home_info_compa` (
  `id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `time` text COLLATE utf8_unicode_ci NOT NULL,
  `map` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_info_compa`
--

INSERT INTO `home_info_compa` (`id`, `address`, `phone`, `email`, `time`, `map`) VALUES
(1, '2', '1', '23', '<ul class=\"stm_schedule_list\"><li class=\"schedule-list_item\"><span class=\"schedule_day\">Th2 - CN:</span><span class=\"schedule_time\">9.30 - 22.00</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\">Lễ - Tết:</span><span class=\"schedule_time\">Mở cửa</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\">Tết Âm lịch:</span><span class=\"schedule_time\">Đóng cửa</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\">Chốt hẹn:</span><span class=\"schedule_time\">8.30 pm</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li></ul>', 'map.png');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `parent` int(11) NOT NULL DEFAULT 0,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `description`, `img`, `active`, `parent`, `slug`, `deleted`, `created_at`, `created_by`) VALUES
(1, 'Gói Trị Liệu', '<p>Tự thưởng cho bản th&acirc;n bằng sự kết hợp của c&aacute;c liệu ph&aacute;p spa để chăm s&oacute;c cơ thể v&agrave; tinh thần. Ch&uacute;ng được thiết kế để khiến bạn b&igrave;nh tĩnh, thoải m&aacute;i v&agrave; xinh đẹp với mức chi ti&ecirc;u hợp l&yacute; nhất.</p>\r\n', '1621895816_tin_tuc_02.jpeg', 1, 0, 'goi-tri-lieu', 0, '2019-05-22 07:07:06', NULL),
(2, 'Làm Mặt', '<p>C&ocirc;ng nghệ độc quyền Darsonval gi&uacute;p b&oacute;c t&aacute;ch nh&acirc;n mụn, loại bỏ dầu thừa v&agrave; b&atilde; nhờn để cải thiện mụn hiệu quả, mang đến l&agrave;n da rạng ngời tr&agrave;n đầy sức sống.</p>\r\n', '1621895310_tin_tuc-04.jpeg', 1, 0, 'lam-mat', 0, '2019-05-22 07:07:06', NULL),
(3, 'Massage Body', '<p><strong>Massage body tinh dầu&nbsp;</strong>l&agrave; phương ph&aacute;p sức khỏe, l&agrave;m đẹp được ưa chuộng từ l&acirc;u. Kh&ocirc;ng chỉ đem tới c&aacute;c gi&acirc;y ph&uacute;t thư gi&atilde;n, thoải m&aacute;i. Liệu tr&igrave;nh n&agrave;y c&ograve;n gi&uacute;p cơ thể của bạn th&ecirc;m săn chắc v&agrave; khỏe mạnh nhiều hơn. Đối với c&aacute;c triệu chứng t&acirc;m l&yacute;, massage cũng gi&uacute;p đưa mọi thứ trở về thể trạng ổn định</p>\r\n', '1621895520_tin_tuc_01.jpeg', 1, 0, 'massage-body', 0, '2019-05-22 07:07:06', NULL),
(4, 'Body Care', '<p>Dịch vụ chăm s&oacute;c body lu&ocirc;ng&nbsp;được kh&aacute;ch h&agrave;ng ưa chuộng, kh&ocirc;ng chỉ kh&aacute;ch h&agrave;ng Việt Nam m&agrave; kh&aacute;ch h&agrave;ng nước ngo&agrave;i cũng rất th&iacute;ch bởi chất lượng v&agrave; kết quả mang lại.&nbsp;H&atilde;y thử trải nghiệm dịch vụ Chăm s&oacute;c cơ thể tại trung t&acirc;m Spa h&agrave;ng đầu TP.HCM nh&eacute;!</p>\r\n', '1621895640_tin_tuc_03.jpeg', 1, 0, 'body-care', 0, '2019-05-22 07:07:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `create_date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `times` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `debt` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `active_time` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `create_date`, `end_date`, `customer_id`, `product_id`, `times`, `paid`, `debt`, `note`, `active`, `active_time`) VALUES
(4, '1623401598', '1623430800', 18, 37, 5, 500, 100, 'no 100', 1, ''),
(5, '1623409059', '1622480400', 19, 37, 10, 300, 100, 'no 100', 1, '0'),
(6, '1623401594', '1623430800', 18, 37, 10, 300, 250, 'nợ 250', 1, ''),
(7, '1623401590', '1623430800', 18, 37, 2, 200, 300, 'no 300', 0, '1623409083'),
(9, '1623409068', '1623430800', 19, 37, 3, 1, 2, 'no 2', 0, '1623409068');

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `date`, `user`, `note`) VALUES
(16, 9, '11-06-2021', 2, '1'),
(17, 9, '11-06-2021', 2, '2'),
(18, 9, '11-06-2021', 2, '3'),
(19, 7, '11-06-2021', 2, '1'),
(20, 7, '12-06-2021', 2, '2');

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `area` int(50) NOT NULL,
  `latitude` int(11) NOT NULL DEFAULT 0,
  `longitude` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 0,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id`, `name`, `address`, `phone`, `area`, `latitude`, `longitude`, `active`, `slug`, `deleted`) VALUES
(1, 'cua hang 1', 'dong nai', '1234', 1, 1, 2, 0, '', 0),
(2, '1', '1', '1', 0, 1, 1, 1, 'cua_hang', 0);

-- --------------------------------------------------------

--
-- Table structure for table `partner_registry`
--

CREATE TABLE `partner_registry` (
  `id` int(11) NOT NULL,
  `representator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `producer`
--

CREATE TABLE `producer` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `producerId` int(11) DEFAULT NULL,
  `img3` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img2` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img1` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `price` int(20) NOT NULL DEFAULT 0,
  `promotion_id` int(11) DEFAULT NULL,
  `inStock` int(11) NOT NULL DEFAULT 0,
  `featured` tinyint(4) NOT NULL DEFAULT 0,
  `hot` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1,
  `img4` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `short_des`, `detail_des`, `category_id`, `producerId`, `img3`, `img2`, `img1`, `event_id`, `price`, `promotion_id`, `inStock`, `featured`, `hot`, `time`, `active`, `img4`, `slug`, `deleted`) VALUES
(1, 'Khăn Vải Không dệt làm sạch phòng ngủ 3', 'Làm sạch và diệt khuẩn phòng ngủ ', '<p>L&agrave;m sạch v&agrave; diệt khuẩn ph&ograve;ng ngủ. Sự điện ph&acirc;n nước được sử dụng trong sản phẩm n&agrave;y được cấu th&agrave;nh từ sự ph&acirc;n giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm l&agrave;m ph&acirc;n giải c&aacute;c chất dơ tr&ecirc;n bề mặt đồ d&ugrave;ng. Ngo&agrave;i ra chất benzalkonium chloride l&agrave; chất c&oacute; t&iacute;nh khử độc v&agrave; diệt khuẩn được sử dụng l&agrave;m sạch m&ocirc;i trường. Quy c&aacute;ch : 48 g&oacute;i / th&ugrave;ng (1g&oacute;i = 20 miếng).</p>\r\n', 1, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 0, NULL, 0, 0, 1, 10, 0, NULL, 'khan-vai-khong-det-lam-sach-phong-ngu-3', 1),
(27, 'Thư giãn vùng vai lưng', 'Thư giãn vùng vai lưng', '<p><a href=\"https://catmocspa.vn/service/thu-gian-vai-lung-30/\">Thư gi&atilde;n v&ugrave;ng vai lưng</a></p>\r\n', 2, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 230000, NULL, 0, 0, 1, 20, 1, NULL, 'thu-gian-vung-vai-lung', 0),
(28, 'Thư giản vùng mặt', 'Thư giản vùng mặt', '<p>Thư giản v&ugrave;ng mặt</p>\r\n', 1, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 290000, NULL, 0, 0, 1, 30, 1, NULL, 'thu-gian-vung-mat', 0),
(29, 'Duy trì làn da tươi trẻ', 'Duy trì làn da tươi trẻ', '<p>Duy tr&igrave; l&agrave;n da tươi trẻ</p>\r\n', 1, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 450000, NULL, 0, 0, 1, 40, 1, NULL, 'duy-tri-lan-da-tuoi-tre', 0),
(32, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(33, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(34, '2', '2', '<p>3</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 2, NULL, 0, 0, 0, 0, 1, NULL, '2', 1),
(35, '2', '2', '<p>3</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 2, NULL, 0, 0, 0, 0, 1, NULL, '2', 1),
(36, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(37, 'Chăm sóc cải thiện làm da nhạy cảm', 'Chăm sóc cải thiện làm da nhạy cảm', '<p>Chăm s&oacute;c cải thiện l&agrave;m da nhạy cảm</p>\r\n', 1, NULL, NULL, NULL, '1621655457_login_background.jpeg', NULL, 590000, NULL, 0, 0, 0, 0, 1, NULL, 'cham-soc-cai-thien-lam-da-nhay-cam', 0),
(38, 'Trị liệu ngăn ngừa lão hóa, làm săn chắc da', 'Trị liệu ngăn ngừa lão hóa, làm săn chắc da', '<p><a href=\"https://catmocspa.vn/service/tri-lieu-ngan-ngua-lao-hoa-lam-san-chac-da-75/\">Trị liệu ngăn ngừa l&atilde;o h&oacute;a, l&agrave;m săn chắc da</a></p>\r\n', 1, NULL, NULL, NULL, '1621949919_login_background.jpeg', NULL, 690000, NULL, 0, 0, 0, 0, 1, NULL, 'tri-lieu-ngan-ngua-lao-hoa-lam-san-chac-da', 0),
(39, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(40, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(41, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(42, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(43, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(44, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(45, '2', '2', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, 0, 0, 0, 1, NULL, '2', 1),
(46, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(47, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(48, 'Trị liệu vết nám, đốm nâu', 'Trị liệu vết nám, đốm nâu', '<p><a href=\"https://catmocspa.vn/service/tri-lieu-vet-nam-dom-nau-80/\">Trị liệu vết n&aacute;m, đốm n&acirc;u</a></p>\r\n', 1, NULL, NULL, NULL, '1621949969_tin_tuc_02.jpeg', NULL, 760000, NULL, 0, 0, 0, 0, 1, NULL, 'tri-lieu-vet-nam-dom-nau', 0),
(49, 'Chăm sóc da mặt thiên nhiên', 'Chăm sóc da mặt thiên nhiên', '<h1>Chăm s&oacute;c da mặt thi&ecirc;n nhi&ecirc;n</h1>\r\n', 1, NULL, NULL, NULL, '1621950034_tin_tuc_03.jpeg', NULL, 360000, NULL, 0, 0, 0, 0, 1, NULL, 'cham-soc-da-mat-thien-nhien', 0),
(50, 'Trị liệu da dầu, mụn đầu đen', 'Trị liệu da dầu, mụn đầu đen', '<p><a href=\"https://catmocspa.vn/service/tri-lieu-da-dau-mun-dau-den-70/\">Trị liệu da dầu, mụn đầu đen</a></p>\r\n', 1, NULL, NULL, NULL, '1621950062_tin_tuc_02.jpeg', NULL, 540000, NULL, 0, 0, 0, 0, 1, NULL, 'tri-lieu-da-dau-mun-dau-den', 0),
(51, 'Trị liệu da mụn bọc, mụn mủ', 'Trị liệu da mụn bọc, mụn mủ', '<p><a href=\"https://catmocspa.vn/service/tri-lieu-da-mun-boc-mun-mu-70/\">Trị liệu da mụn bọc, mụn mủ</a></p>\r\n', 1, NULL, NULL, NULL, '1621950091_tin_tuc-04.jpeg', NULL, 580000, NULL, 0, 0, 0, 0, 1, NULL, 'tri-lieu-da-mun-boc-mun-mu', 0),
(52, 'Liệu pháp Vitamin C cho làn da sáng khỏe', 'Liệu pháp Vitamin C cho làn da sáng khỏe', '<p><a href=\"https://catmocspa.vn/service/lieu-phap-vitamin-c-cho-lan-da-sang-khoe-60/\">Liệu ph&aacute;p Vitamin C cho l&agrave;n da s&aacute;ng khỏe</a></p>\r\n', 1, NULL, NULL, NULL, '1621950128_tin_tuc_02.jpeg', NULL, 730000, NULL, 0, 0, 0, 0, 1, NULL, 'lieu-phap-vitamin-c-cho-lan-da-sang-khoe', 0),
(53, 'Trị liệu tái sinh và trẻ hóa làn da', 'Trị liệu tái sinh và trẻ hóa làn da', '<p><a href=\"https://catmocspa.vn/service/tri-lieu-tai-sinh-va-tre-hoa-lan-da-80/\">Trị liệu t&aacute;i sinh v&agrave; trẻ h&oacute;a l&agrave;n da</a></p>\r\n', 1, NULL, NULL, NULL, '1621950164_tin_tuc_03.jpeg', NULL, 860000, NULL, 0, 0, 0, 0, 1, NULL, 'tri-lieu-tai-sinh-va-tre-hoa-lan-da', 0),
(54, 'Liệu pháp Thái', 'Liệu pháp Thái', '<p><a href=\"https://catmocspa.vn/service/lieu-phap-thai-60/\">Liệu ph&aacute;p Th&aacute;i</a></p>\r\n', 2, NULL, NULL, NULL, '1621951583_tin_tuc_02.jpeg', NULL, 360000, NULL, 0, 0, 0, 0, 1, NULL, 'lieu-phap-thai', 0),
(57, 'Liệu pháp Thụy Điển', 'Liệu pháp Thụy Điển', '<p><a href=\"https://catmocspa.vn/service/lieu-phap-thuy-dien-90/\">Liệu ph&aacute;p Thụy Điển</a></p>\r\n', 2, NULL, NULL, NULL, '1621951455_tin_tuc_03.jpeg', NULL, 450000, NULL, 0, 0, 0, 0, 1, NULL, 'lieu-phap-thuy-dien', 0),
(58, 'Thư giãn đá nóng với tinh dầu', 'Thư giãn đá nóng với tinh dầu', '<p><a href=\"https://catmocspa.vn/service/thu-gian-da-nong-voi-tinh-dau-90/\">Thư gi&atilde;n đ&aacute; n&oacute;ng với tinh dầu</a></p>\r\n', 2, NULL, NULL, NULL, '1621951491_tin_tuc-04.jpeg', NULL, 480000, NULL, 0, 0, 0, 0, 1, NULL, 'thu-gian-da-nong-voi-tinh-dau', 0),
(59, 'Liệu pháp Thảo Dược', 'Liệu pháp Thảo Dược', '<p><a href=\"https://catmocspa.vn/service/lieu-phap-thao-duoc-90/\">Liệu ph&aacute;p Thảo Dược</a></p>\r\n', 2, NULL, NULL, NULL, '1621951514_tin_tuc-04.jpeg', NULL, 560000, NULL, 0, 0, 0, 0, 1, NULL, 'lieu-phap-thao-duoc', 0),
(64, 'Thanh tẩy toàn thân', 'Thanh tẩy toàn thân', '<p><a href=\"https://catmocspa.vn/service/thanh-tay-toan-than-30/\">Thanh tẩy to&agrave;n th&acirc;n</a></p>\r\n', 3, NULL, NULL, NULL, '1621951782_tin_tuc_03.jpeg', NULL, 230000, NULL, 0, 0, 0, 0, 1, NULL, 'thanh-tay-toan-than', 0),
(65, 'Chăm sóc ngót hồng', 'Chăm sóc ngót hồng', '<p><a href=\"https://catmocspa.vn/service/cham-soc-got-hong-30/\">Chăm s&oacute;c ng&oacute;t hồng</a></p>\r\n', 3, NULL, NULL, NULL, '1621951808_tin_tuc_03.jpeg', NULL, 220000, NULL, 0, 0, 0, 0, 1, NULL, 'cham-soc-ngot-hong', 0),
(66, 'Trẻ hóa da tay với Paraffin', 'Trẻ hóa da tay với Paraffin', '<p><a href=\"https://catmocspa.vn/service/tre-hoa-da-tay-voi-paraffin-30/\">Trẻ h&oacute;a da tay với Paraffin</a></p>\r\n', 3, NULL, NULL, NULL, '1621951881_tin_tuc_02.jpeg', NULL, 190000, NULL, 0, 0, 0, 0, 1, NULL, 'tre-hoa-da-tay-voi-paraffin', 0),
(67, 'Waxing tay', 'Waxing tay', '<p><a href=\"https://catmocspa.vn/service/waxing-tay-30/\">Waxing tay</a></p>\r\n', 3, NULL, NULL, NULL, '1621952179_tin_tuc_02.jpeg', NULL, 270000, NULL, 0, 0, 0, 0, 1, NULL, 'waxing-tay', 0),
(68, 'Waxing nguyên chân', 'Waxing nguyên chân', '<p><a href=\"https://catmocspa.vn/service/waxing-nguyen-chan-40/\">Waxing nguy&ecirc;n ch&acirc;n</a></p>\r\n', 3, NULL, NULL, NULL, '1621952211_tin_tuc_02.jpeg', NULL, 360000, NULL, 0, 0, 0, 0, 1, NULL, 'waxing-nguyen-chan', 0),
(69, 'Mặt nạ toàn thân', 'Mặt nạ toàn thân', '<p><a href=\"https://catmocspa.vn/service/mat-na-toan-than-30/\">Mặt nạ to&agrave;n th&acirc;n</a></p>\r\n', 3, NULL, NULL, NULL, '1621952249_tin_tuc-04.jpeg', NULL, 210000, NULL, 0, 0, 0, 0, 1, NULL, 'mat-na-toan-than', 0),
(70, 'Trẻ hóa da chân với Paraffin', 'Trẻ hóa da chân với Paraffin', '<p><a href=\"https://catmocspa.vn/service/tre-hoa-da-chan-voi-paraffin-40/\">Trẻ h&oacute;a da ch&acirc;n với Paraffin</a></p>\r\n', 3, NULL, NULL, NULL, '1621952278_tin_tuc_02.jpeg', NULL, 250000, NULL, 0, 0, 0, 0, 1, NULL, 'tre-hoa-da-chan-voi-paraffin', 0),
(71, 'Waxing vùng nách', 'Waxing vùng nách', '<p><a href=\"https://catmocspa.vn/service/waxing-vung-nach-15/\">Waxing v&ugrave;ng n&aacute;ch</a></p>\r\n', 3, NULL, NULL, NULL, '1621952302_tin_tuc_02.jpeg', NULL, 180000, NULL, 0, 0, 0, 0, 1, NULL, 'waxing-vung-nach', 0),
(72, 'Waxing nửa chân', 'Waxing nửa chân', '<p><a href=\"https://catmocspa.vn/service/waxing-nua-chan-30/\">Waxing nửa ch&acirc;n</a></p>\r\n', 3, NULL, NULL, NULL, '1621952364_tin_tuc_02.jpeg', NULL, 300000, NULL, 0, 0, 0, 0, 1, NULL, 'waxing-nua-chan', 0),
(73, 'Gội đầu kiểu Nhật với L’oreal thảo dược', 'Gội đầu kiểu Nhật với L’oreal thảo dược', '<p><a href=\"https://catmocspa.vn/service/goi-dau-kieu-nhat-voi-loreal-35/\">Gội đầu kiểu Nhật với L&rsquo;oreal thảo dược</a></p>\r\n', 3, NULL, NULL, NULL, '1621952391_tin_tuc-04.jpeg', NULL, 130000, NULL, 0, 0, 0, 0, 1, NULL, 'goi-dau-kieu-nhat-voi-loreal-thao-duoc', 0),
(74, 'Tái tạo năng lượng', 'Tái tạo năng lượng', '<p><a href=\"https://catmocspa.vn/service/tai-tao-nang-luong-15/\">T&aacute;i tạo năng lượng</a></p>\r\n', 4, NULL, NULL, NULL, '1621952435_tin_tuc_03.jpeg', NULL, 388000, NULL, 0, 0, 0, 0, 1, NULL, 'tai-tao-nang-luong', 0),
(75, 'Thư giãn cơ thể', 'Thư giãn cơ thể', '<p><a href=\"https://catmocspa.vn/service/renewal-2-5h/\">Thư gi&atilde;n cơ thể</a></p>\r\n', 4, NULL, NULL, NULL, '1621952462_tin_tuc_01.jpeg', NULL, 578000, NULL, 0, 0, 0, 0, 1, NULL, 'thu-gian-co-the', 0),
(76, 'Chăm sóc cơ thể', 'Chăm sóc cơ thể', '<p><a href=\"https://catmocspa.vn/service/travellers-relaxation-2h/\">Chăm s&oacute;c cơ thể</a></p>\r\n', 4, NULL, NULL, NULL, '1621952490_tin_tuc-04.jpeg', NULL, 418000, NULL, 0, 0, 0, 0, 1, NULL, 'cham-soc-co-the', 0),
(77, 'Khỏe đẹp toàn diện', 'Khỏe đẹp toàn diện', '<p><a href=\"https://catmocspa.vn/service/revitalization-3h/\">Khỏe đẹp to&agrave;n diện</a></p>\r\n', 4, NULL, NULL, NULL, '1621952514_tin_tuc_02.jpeg', NULL, 728000, NULL, 0, 0, 0, 0, 1, NULL, 'khoe-dep-toan-dien', 0);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `starDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `discount` float NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `gift_productId` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `name`, `description`, `starDate`, `endDate`, `discount`, `type`, `img`, `gift_productId`, `deleted`) VALUES
(1, 'asd', 'asdas', '2019-04-23 00:00:00', '2019-04-30 00:00:00', 1235, 0, 'promotion.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `repcomment`
--

CREATE TABLE `repcomment` (
  `id` int(11) NOT NULL,
  `commentId` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SEO`
--

CREATE TABLE `SEO` (
  `id` int(11) NOT NULL,
  `page` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SEO`
--

INSERT INTO `SEO` (`id`, `page`, `meta`) VALUES
(1, 'Trang chủ', '123'),
(2, 'Sản phẩm', '1123'),
(3, 'Hình ảnh', '2'),
(4, 'Tin tức', '3'),
(5, 'Về chúng tôi', '4'),
(6, 'Đối tác', '5'),
(7, 'Danh mục', '7');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `full_name`, `email`, `phone`, `role`, `deleted`) VALUES
(2, 'admin', 'b1b690c6d73e50010579dc3fa65a8bcacc2229c8', 'qưe', 'hongoctuan.it@gmail.com', '090909', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `category` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `category` ADD FULLTEXT KEY `name_2` (`name`,`description`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commentProduct` (`productId`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detaildistributionDitributon` (`distributionId`),
  ADD KEY `fk_detaildistributionProduct` (`productId`);

--
-- Indexes for table `distribution`
--
ALTER TABLE `distribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_distributionDistributor` (`distributorId`),
  ADD KEY `fk_distributionUser` (`userId`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banner`
--
ALTER TABLE `home_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `news` ADD FULLTEXT KEY `name` (`name`,`description`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name` (`name`,`address`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name_2` (`name`,`address`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name_3` (`name`,`address`);

--
-- Indexes for table `partner_registry`
--
ALTER TABLE `partner_registry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producer`
--
ALTER TABLE `producer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producer` (`producerId`),
  ADD KEY `fk_category` (`category_id`),
  ADD KEY `fk_event` (`event_id`),
  ADD KEY `fk_promotion` (`promotion_id`);
ALTER TABLE `product` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `product` ADD FULLTEXT KEY `name_2` (`name`,`detail_des`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repcomment`
--
ALTER TABLE `repcomment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_repcommentComment` (`commentId`);

--
-- Indexes for table `SEO`
--
ALTER TABLE `SEO`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distribution`
--
ALTER TABLE `distribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distributor`
--
ALTER TABLE `distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `home_banner`
--
ALTER TABLE `home_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partner_registry`
--
ALTER TABLE `partner_registry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `producer`
--
ALTER TABLE `producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `repcomment`
--
ALTER TABLE `repcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `SEO`
--
ALTER TABLE `SEO`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_commentProduct` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Constraints for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  ADD CONSTRAINT `fk_detaildistributionDitributon` FOREIGN KEY (`distributionId`) REFERENCES `distribution` (`id`),
  ADD CONSTRAINT `fk_detaildistributionProduct` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Constraints for table `distribution`
--
ALTER TABLE `distribution`
  ADD CONSTRAINT `fk_distributionDistributor` FOREIGN KEY (`distributorId`) REFERENCES `distributor` (`id`),
  ADD CONSTRAINT `fk_distributionUser` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `fk_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `fk_producer` FOREIGN KEY (`producerId`) REFERENCES `producer` (`id`),
  ADD CONSTRAINT `fk_promotion` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`);

--
-- Constraints for table `repcomment`
--
ALTER TABLE `repcomment`
  ADD CONSTRAINT `fk_repcommentComment` FOREIGN KEY (`commentId`) REFERENCES `comment` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
