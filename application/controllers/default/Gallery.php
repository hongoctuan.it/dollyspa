<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gallery extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->gallery_cate = isset($_GET['gallery_cate']) ? $this->input->get('gallery_cate') : false;
		$this->M_myweb->set_table('gallery');
		$this->load->model('default/m_gallery');
		$this->load->model('default/m_seo');
		$this->data['meta']  = $this->m_seo->getSEO(3);
		$this->data['com_info'] = $this->M_myweb->set_table('home_info_compa')->gets()[0];
		$this->data['intro'] = $this->M_myweb->set_table('about')->gets()[0];
	}

	public function index()
	{
		$this->data['gallery_datas'] = $this->m_gallery->getGalleryList();
		$this->data['title'] = "Hình Ảnh";
		$this->data['subview'] = 'default/gallery/V_gallery';
		$this->load->view('default/_main_page', $this->data);
	}
	public function album($album)
	{
		$this->Model = $this->M_myweb->set_table('gallery_detail');

		$this->data['obj'] = $this->Model->set('id',$album)->get();
		$this->data['promotions'] = $this->Model->set('id <>',$album)->setLimit(5)->gets();
		if (!empty($this->data['obj'])){
			$this->data['subview'] = 'default/gallery/V_gallery_page';
			$this->load->view('default/_main_page', $this->data);
		}else{
			redirect ('khuyen-mai');
		}

	}
	public function getPageAjax()
	{
		$page = $_POST['page'];
		if(isset($page))
		{
			$category = $_POST['category']!=0?$_POST['category']:false;
			$this->m_gallery->setPage($page);
			$this->data['images'] = $this->m_gallery->loadImagePage($category);
			if($this->data['images'])
			{
				$this->load->view('default/gallery/V_gallery_page',$this->data);
			}
		}else{
			return false;
		}
	}
}