<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('about');
		$this->load->model('default/m_seo');
	}
	
	public function index()
	{
		$this->data['meta']  = $this->m_seo->getSEO(5);
		//general
		$introduce_info = $this->Model->get();
		$this->data['introduce_info'] = $introduce_info;
		//history
		$this->Model = $this->M_myweb->set('deleted',0)->set_table('history');
		$this->data['history'] 	= $this->Model->set_orderby('title')->gets();
		$this->data['title']	= "Về Chúng Tôi";
		$this->data['subview'] 	= 'default/about/V_about';
		// print_r($this->data['introduce_info']);
		$this->load->view('default/_main_page',$this->data);
	}
}