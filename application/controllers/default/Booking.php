<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends MY_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->data['page']=isset($_GET['page'])?$_GET['page']:1;
		$this->data['sort']=isset($_GET['sort'])?$_GET['sort']:false;
		$this->cate=isset($_GET['cate'])?$this->input->get('cate'):false;
		$this->data['com_info'] = $this->M_myweb->set_table('home_info_compa')->gets()[0];
		$this->data['intro'] = $this->M_myweb->set_table('about')->gets()[0];
        $this->load->model('default/m_booking');
	}
	
	public function index()
	{
		$this->Model = $this->M_myweb->set_table('product');
		$this->data['products'] = $this->Model->set('deleted',0)->set_orderby('name')->select("id, name")->gets();
		$this->data['subview'] = 'default/booking/V_booking';
		$this->load->view('default/_main_page',$this->data);
	}

	public function save(){
		$data = $this->input->post();
        $this->Model = $this->M_myweb->set_table('customer');
        $customer['name'] = $data['name'];
        $customer['phone'] = $data['phone'];
		$customer['create_date'] = time();
		$customer['type'] = 1;
        $this->Model->sets($customer)->save();
        $customer_id = $this->m_booking->getMaxId()['id'];
        $booking['note'] = $data['note'];
		$date = new DateTime($data['date']);
		$date = $date->getTimestamp();
        $booking['date'] = $date;
        $booking['time'] = $data['time'];
		$booking['product'] = $data['product'];
        $booking['number'] = $data['number'];
        $booking['note'] = $data['note'];
        $booking['customer_id']=$customer_id;
        $this->Model = $this->M_myweb->set_table('booking');
        $this->Model->sets($booking)->save();
		$_SESSION['system_msg'] = messageDialog("div","success","Đặt lịch thành công");
		return redirect(site_url('dat-cho'));
	}
}