<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('obisys');
		$this->load->model('M_myweb');
	}
	
	public function index()
	{
		if($this->input->post()){
			$this->login();
		}
		$this->load->view('cms/auth/home');
	}

	public function logout(){
		$_SESSION['system_msg'] = messageDialog('div', 'success', 'Đã đăng xuất khỏi hệ thống');
		unset($_SESSION['system']);
		return redirect(site_url("login"));
	}

	public function login(){
		$data = $this->input->post();
		$get = $this->M_myweb->set('username',$data['username'])->set('password',hashpass($data['pass']))->set_table('user')->get();
		if($get){
			$session = array(
				'id'			=> $get->id,
				'logged_in'		=>	true,
				'userName'		=>	$get->userName,
				'fullName'		=>	$get->fullName,
				'avatar'		=>	$get->avatar,
				'role'			=>	$get->role,
				'email'			=>	$get->email,
				'logid'			=>	$get->id,
				'token'			=>	randomString(30)
			);
			$_SESSION['system'] = (object)$session;
			return redirect(site_url('admin/analytic'));
		}else{
			$_SESSION['system_msg'] = messageDialog('div', 'error', 'Tên đăng nhập hoặc tài khoản không chính xác');
			return redirect(site_url("login"));
		}
	}
}