<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class homeInfoCompa extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('home_info_compa');
	}
	
	public function index()
	{
		
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['obj'] = $this->Model->get();
		$this->data['subview'] = 'cms/hotproductinfo/homeInfoCompa';
		$this->load->view('cms/_main_page',$this->data);
  }
    
  private function save(){
	  	$data = $this->input->post();
		$this->Model->sets($data)->setPrimary(1)->save();
		$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thành công");
		return redirect(site_url('admin/homeInfoCompa'));
	}
}