<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['product'] = false;
		$this->data['cates'] = $this->M_myweb->set('deleted',0)->set_table('category')->gets();
		$this->Model = $this->M_myweb->set_table('product');
		$this->load->model('cms/m_product');
		$this->load->library('pagination');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "hot":
				$this->hot();
				break;
			case "unhot":
				$this->unHot();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			case "del":
				$this->delete();
				break;
			case "child_list":
				$this->child_list();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data["product"] = $this->m_product->get_current_page_records();
		$this->data['subview'] = 'cms/product/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function createLinks($start) {
		
		$page_curent = ceil( $start / $this->_limit )+1;
		$page = ceil($this->_total/$this->_limit);
		$last = ceil( $this->_total / $this->_limit );	
		$html = '<ul>';
		if($page_curent < $page-1){
			$html .= '<li style="list-style:none; float:left"><a class="btn btn-info" style="text-decoration: none;" href="'.site_url('admin/product?paging='.($page_curent)*$this->_limit).'">Sau</a></li>'; 
		}
		for($i=1; $i<=$page; $i++){
			if($page_curent == $i){
				$html .= '<li class="btn" style="color:red;list-style:none; float:left; font-weight: bold;">'.$i.'</li>'; 
			}else{
				$html .= '<li style="list-style:none; float:left"><a class="btn" style="text-decoration: none; font-weight: bold;" href="'.site_url('admin/product?paging='.($i-1)*$this->_limit).'">'.$i.'</a></li>'; 
			}
		}
		if($page_curent != 1){
			$html .= '<li style="list-style:none; float:left"><a class="btn btn-info" style="text-decoration: none;" href="'.site_url('admin/product?paging='.($page_curent-2)*$this->_limit).'">Trước</a></li>'; 
		}
		$html .= '</ul>'; 
		return $html;
	}

	private function edit(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
			$this->data['obj']->image_01 = "";
		}
		if(isset($_GET['cat'])){
			$this->data['cat'] = $_GET['cat'];
		}
		$this->data['subview'] = 'cms/product/edit';
		$this->load->view('cms/_main_page',$this->data);
	}
	
	private function hot(){
		if(isset($_GET['id'])){
			$data['id'] = $_GET['id'];
			$data['hot'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function unHot(){
		if(isset($_GET['id'])){
			$data['id'] = $_GET['id'];
			$data['hot'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function save(){
		$data = $this->input->post();
		$image_01 = "";
		if($_FILES['image_01']['name']!=""){
			$image_01 = do_upload('avatar','image_01');	
			$data['img1'] = $image_01;				
		}
		if(!isset($data['slug'])||trim($data['slug'])==""){
			$data['slug'] = getCleanSlug(removeVnChar($data['name']));
		}else{
			$data['slug'] = getCleanSlug(removeVnChar($data['slug']));
		}
		if($this->id){
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật sản phẩm thành công");
		}else{
			$id=$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm sản phẩm thành công");
		}
		return redirect(site_url('admin/product?act=child_list&id='.$data['category_id'].'&token='.$this->data['infoLog']->token));
	}

	private function delete(){
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá sản phẩm thành công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá sản phẩm");
			}
		}
		redirect(site_url('admin/product'));
	}

	private function child_list(){
		$this->id = $_GET['id'];
		$this->data["product"] = $this->m_product->loadProductPage($this->id);
		$this->data["category"] = $this->id;
		$this->data['subview'] = 'cms/product/home';
		$this->load->view('cms/_main_page',$this->data);
	}
}