<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Analytic extends CMS_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->data['promotions'] = $this->M_myweb->set_table('promotion')->gets();
		$this->Model = $this->M_myweb->set_table('customer');
		$this->load->model('cms/m_analytic');
	}

	public function index()
	{
		switch ($this->act) {
			default:
				$this->home();
				break;
		}
	}

	private function home()
	{
		$this->data['total_customer_active'] = $this->M_myweb->set("type",0)->set("active",1)->counts();
		$this->data['total_customer_inactive'] = $this->M_myweb->set("type",1)->set("active",1)->counts();
		$this->data['total_paid'] = $this->m_analytic->getPaid();
		$this->data['total_debt'] = $this->m_analytic->getDebt();
		$this->data['customerMonthByMonth'] = $this->m_analytic->getMonthByMonthCustomer();
		$this->data['incomeByMonth']= $this->m_analytic->getIncomeMonthByMonth();
		$this->data['totalActiveCustomer']= $this->m_analytic->getTotalIncomeCustomer();
		$this->data['totalExpireCustomer']= $this->m_analytic->getTotalExpireCustomer();
		$this->data['expireCustomer']= $this->m_analytic->getExpireCustomer();
		$this->data['customerInfoOfMonth']= $this->m_analytic->getCustomerInfoOfMonth();
		$this->data['subview'] = 'cms/analytic/home';
		$this->load->view('cms/_main_page', $this->data);
	}

	
}
