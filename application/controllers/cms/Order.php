<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('cms/m_order');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "upd_package":
				if($this->input->post())
					$this->save_package();
				$this->edit_package();
				break;
			case "upd_customer":
				if($this->input->post())
					$this->save_package();
				$this->edit_customer();
				break;
			case "save_package":
				$this->save_package();
				break;
			case "del":
				$this->delete();
				break;
			case "child_list":
				$this->child_list();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$arr = array();
		$temps = $this->m_order->getOrder();
		$before_customerId = $temps[0]->customer_id;
		$flag = false;
		foreach($temps as $temp){
			if($before_customerId == $temp->customer_id){
				if($flag==false){
					$temp->flag= true;
					$temp->rowspan = $this->listOperation($temp->customer_id, $temps);
					$arr[] = $temp;
					$flag=true;
				}else{
					$temp->flag= false;
					$temp->rowspan = $this->listOperation($temp->customer_id, $temps);
					$arr[] = $temp;
				}
				
			}
			else{
				$before_customerId = $temp->customer_id;
				$temp->flag= true;
				$temp->rowspan = $this->listOperation($temp->customer_id, $temps);
				$arr[] = $temp;
				$flag=true;
			}
		}
		$this->data['order'] = $arr;
		$this->data['subview'] = 'cms/order/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function listOperation($customerId, $orderList){
		$dem=0;
		foreach($orderList as $order){
			if($customerId == $order->customer_id){
				$dem++;
			}
		}
		return $dem;
	}

	private function edit(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
			$this->data['obj']->image_01 = "";
		}
		$this->Model = $this->M_myweb->set_table('product');
		$this->data['products'] = $this->Model->set('deleted',0)->set_orderby('name')->select("id, name")->gets();
		$this->data['subview'] = 'cms/order/edit';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function edit_package(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->m_order->getOrderPackage($_GET['id'])[0];
			$this->data['obj_details'] = $this->m_order->getOrderPackageDetail($_GET['id']);
		}
		$this->Model = $this->M_myweb->set_table('product');
		$this->data['products'] = $this->Model->set('deleted',0)->set_orderby('name')->select("id, name")->gets();
		$this->data['subview'] = 'cms/order/edit_package';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function edit_customer(){

		if(isset($_GET['id'])){
			$this->Model = $this->M_myweb->set_table('customer');
			$this->data['obj'] = $this->Model->set('id',$_GET['id'])->set_orderby('name')->gets()[0];
			$this->data['obj_details'] = $this->m_order->getOrderList($this->data['obj']->id);
			$this->data['subview'] = 'cms/order/customer';
			$this->load->view('cms/_main_page',$this->data);
		}
		else{
			$this->data['subview'] = 'admin/order';
			$this->load->view('cms/_main_page',$this->data);
		}
	}

	private function save_package(){
	
		$data = $this->input->post();
		$this->Model = $this->M_myweb->set_table('order');
		$times_order = $this->Model->set('id',$data['order_id'])->get()->times;
		$this->Model = $this->M_myweb->set_table('order_detail');
		$this->Model->sets($data)->save();
		$times_orderdetail = $this->M_myweb->set('order_id',$data['order_id'])->counts();
		if($times_order == $times_orderdetail){
			$this->Model = $this->M_myweb->set_table('order');
			$active['active'] = 0;
			$active['active_time'] = time();
			$this->Model->sets($active)->setPrimary($data['order_id'])->save();
		}
		return redirect(site_url('admin/order?act=upd_package&id='.$data['order_id'].'&token='.$this->data['infoLog']->token));
	}


	private function save(){
		$data = $this->input->post();
		$this->Model = $this->M_myweb->set_table('customer');
		$customer['name'] = $data['name'];
		$customer['phone'] = $data['phone'];
		$customer['create_date'] = time();
		$customer['type'] = 0;
		$checkExist = $this->Model->set('phone',$customer['phone'])->get()->id;
		if(!empty($checkExist)){
			$customer_id = $checkExist;
		}else{
			$this->Model->sets($customer)->setPrimary($this->id)->save();
			$customer_id = $this->m_order->getMaxId()['id'];
		}
		$this->Model = $this->M_myweb->set_table('order_detail');
		$times_orderdetail = $this->Model->set('order_id',$this->id)->counts();
		$this->Model = $this->M_myweb->set_table('order');
		if($times_orderdetail < $data['times']){
			$order['active'] = 1;
			$order['active_time'] = 0;
		}else{
			$order['active'] = 0;
			$order['active_time'] = time();
		}
		$end_date = new DateTime($data['end_date']);
		$end_date = $end_date->getTimestamp();
		$order['end_date'] = $end_date;
		$order['customer_id'] = $customer_id;
		$order['product_id'] = $data['product_id'];
		$order['times'] = $data['times'];
		$order['create_date'] = time();
		$order['paid'] = $data['paid'];
		$order['debt'] = $data['debt'];
		$order['note'] = $data['note'];
		$this->Model->sets($order)->setPrimary($this->id)->save();
		return redirect(site_url('admin/order'));
	}


	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model = $this->M_myweb->set_table('order');
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		return redirect(site_url('admin/order'));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model = $this->M_myweb->set_table('order');
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		return redirect(site_url('admin/order'));
	}
}