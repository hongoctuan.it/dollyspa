<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CMS_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->model('cms/m_order');
	}
	
	public function index()
	{
        switch($this->act){
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			default:
				$this->home();
				break;
		}
        
	}

    private function home(){
        $value = $_GET['value'];
        
        if(isset($value)){
            $arr = array();
            $temps = $this->m_order->searchOrderLike("c.name",$value);
            if(empty($temps)){
                $temp = $this->m_order->searchOrderLike("c.phone",$value);        
            }
            $before_customerId = $temps[0]->customer_id;
            $flag = false;
            foreach($temps as $temp){
                if($before_customerId == $temp->customer_id){
                    if($flag==false){
                        $temp->flag= true;
                        $temp->rowspan = $this->listOperation($temp->customer_id, $temps);
                        $arr[] = $temp;
                        $flag=true;
                    }else{
                        $temp->flag= false;
                        $temp->rowspan = $this->listOperation($temp->customer_id, $temps);
                        $arr[] = $temp;
                    }
                    
                }
                else{
                    $before_customerId = $temp->customer_id;
                    $temp->flag= true;
                    $temp->rowspan = $this->listOperation($temp->customer_id, $temps);
                    $arr[] = $temp;
                    $flag=true;
                }
            }
            $this->data['value'] = $value;
            $this->data['order'] = $arr;
            $this->data['subview'] = 'cms/search/home';
            $this->load->view('cms/_main_page',$this->data);
            
        }else{
            return redirect(site_url('admin/order'));
        }
    }

    private function listOperation($customerId, $orderList){
		$dem=0;
		foreach($orderList as $order){
			if($customerId == $order->customer_id){
				$dem++;
			}
		}
		return $dem;
	}

    private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model = $this->M_myweb->set_table('order');
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		return redirect(site_url('admin/search?control=order&value='.$_GET['value']));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model = $this->M_myweb->set_table('order');
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		return redirect(site_url('admin/search?control=order&value='.$_GET['value']));
	}
    
}