<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Introduce extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('about');
	}
	
	public function index()
	{
		
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				break;
			case "history":
				$this->history();
				break;
			case "upd_history":
				$this->updateHistory();
				break;
			case "new_history":
				$this->newHistory();
				break;
			case "del":
				$this->delHistory();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['obj'] = $this->Model->get();
		$this->data['subview'] = 'cms/introduce/general_intro';
		$this->load->view('cms/_main_page',$this->data);
  }
    
  private function save(){
		$data = $this->input->post();
		$image_01 = "";
		$arr= array();
		if($_FILES['image_01']['name']!=""){
			$image_01 = do_upload('avatar','image_01');	
			$data['img']=$image_01;
		}
		$data['slug'] = str_replace(" ","_",$data['slug']);
		$this->Model->sets($data)->setPrimary(1)->save();
		$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thành công");
		return redirect(site_url('admin/introduce'));
	}
	private function history(){
		$this->Model = $this->M_myweb->set_table('history');
		$this->data['history'] = $this->Model->set('deleted',0)->set_orderby('title')->gets();
		$this->data['subview'] = 'cms/introduce/history_list';
		$this->load->view('cms/_main_page',$this->data);
	}
	private function updateHistory(){
		$this->Model = $this->M_myweb->set_table('history');
		$data = $this->input->post('History');
		$id = $_GET['id'];
		if(!empty($data)){
			$image_01 = "";
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');			
				$data['img'] = $image_01;		
			}
			$this->Model->sets($data)->setPrimary($id)->save();
			return redirect(site_url('admin/introduce?act=history&token='.$this->data['infoLog']->token));
		}else{
			$this->data['id'] = $id;
			$this->data['obj'] =  $this->Model->set('id',$this->data['id'])->get();
			$this->data['subview'] = 'cms/introduce/history_edit';
			$this->load->view('cms/_main_page',$this->data);
		}
	}
	private function newHistory(){
		$this->Model = $this->M_myweb->set_table('history');
		$data = $this->input->post('History');
		if(!empty($data)){
			$image_01 = "";
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');		
				$data['img']=$image_01;
			}
			$this->Model->sets($data)->save();
			return redirect(site_url('admin/introduce?act=history&token='.$this->data['infoLog']->token));
		}else{
				$this->data['subview'] = 'cms/introduce/history_edit';
			//print_r($this->data['history']);
			$this->load->view('cms/_main_page',$this->data);
		}
	}
	private function delHistory(){
		$this->Model = $this->M_myweb->set_table('history');
		$id = $_GET['id'];
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['deleted'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		return redirect(site_url('admin/introduce?act=history&token='.$this->data['infoLog']->token));
	}
}