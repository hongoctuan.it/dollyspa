<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->search = isset($_GET['search']) ? $_GET['search'] : FALSE;
		$this->load->model('default/m_seo');
		$this->load->model('default/M_nguyenquan',"m_nguyenquan");
		$this->load->model('default/M_news',"m_news");
		$this->load->model('default/M_category',"m_category");
		$this->data['company_info'] = $this->m_nguyenquan->getCompanyInfo();
		$this->data['intro'] = $this->M_myweb->set_table('about')->gets()[0];
	}
	public function index()
	{
		$this->data['meta']  = $this->m_seo->getSEO(1);
		$this->data['banners'] = $this->M_myweb->set_table('home_banner')->sets(array('deleted' => 0, 'active' => 1))->set_orderby('id','desc')->gets();
		$this->data['categories'] = $this->M_myweb->set_table('category')->sets(array('deleted' => 0, 'active' => 1, 'level' => 0))->gets();
		$this->data['products'] = $this->M_myweb->set_table('product')->sets(array('deleted' => 0, 'active' => 1, 'hot' => 1))->gets();
		$this->data['promotions'] = $this->M_myweb->set_table('gallery_detail')->sets(array('deleted' => 0, 'active' => 1, 'end_date >' => time()))->setLimit(2)->set_orderby('id','desc')->gets();
		$this->data['news'] = $this->M_myweb->set_table('news')->sets(array('deleted' => 0, 'active' => 1))->set_orderby('id','desc')->gets();
		foreach($this->data['categories'] as $key=>$category){
			$this->m_category->setLimit(12);
			$this->m_category->setOrderBy('hot','desc');
			$this->data['categories'][$key]->products = $this->m_category->loadProductPage($category->id);
		}
		$this->data['title']	= "Trang Chủ";
		$this->data['subview'] 	= 'default/index/V_index';
		$this->load->view('default/_main_page', $this->data);
	}
	public function search()
	{
		if ($this->search) {
			$this->data['meta']  = $this->m_seo->getSEO(5);
			$search = explode(" ", $this->search);
			$this->data['title'] = "Tìm Kiếm";
			$this->data['search'] = $this->m_nguyenquan->getSearchData($this->search);
			$this->data['subview'] 	= 'default/search/V_search';
			$this->load->view('default/_main_page', $this->data);
		} else {
			redirect(site_url());
		}
	}
	public function partner_registry()
	{
		$data = $this->input->post();
		if ($data) {
			$this->M_myweb->set_table('partner_registry')->sets($data)->save();
			echo 1;
		} else {
			echo 0;
		}
	}
}
