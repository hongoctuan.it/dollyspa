<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

    public $data = array();
	public function __construct() {
		parent::__construct();
		$this->load->helper('obisys');
        $this->load->model('M_myweb');
        $this->load->model('default/M_nguyenquan',"m_nguyenquan");
        $this->data['company_info'] = $this->m_nguyenquan->getCompanyInfo();
    }
    

}