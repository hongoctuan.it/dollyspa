
<div class="app-main__inner">
	<div class="app-page-title">
        <div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Danh sách khuyến mãi</div>
			</div>
            <div class="page-title-actions">
				<div class="d-inline-block dropdown">
					<a href="<?php echo site_url('admin/gallery_detail?act=upd&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
						
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Khuyến Mãi
						</button>
					</a>
				</div>
			</div>    
        </div>
    </div>           
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
							<th>#</th>
							<th>Tên khuyến mãi</th>
							<th>Trạng thái</th>
							<th style="width:30%">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($gallery_detail):
							foreach($gallery_detail as $key=>$obj){
						?>
							<tr>
								<th scope="row"><?php echo $key+1?></th>
								<td>
									<a href="<?php echo site_url('admin/gallery_detail?act=upd&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<?php echo $obj->name?>
									</a>
								</td>
								<?php if($obj->end_date > time()): ?>
									<td class="badge badge-info">
										Còn hạn
									</td>
								<?php else: ?>
									<td class="badge badge-warning">
										Còn hạn
									</td>
								<?php endif ?>
								<td>
								<a href="<?php echo site_url('admin/gallery_detail?act=upd&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<span class="nav-icon">
											<i class="fa fa-fw fa-edit "></i>
										</span>
									</a>
									<?php if($obj->active==1):?>
										<a href="<?php echo site_url('admin/gallery_detail?act=lock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-unlock-alt "></i>
											</span>
										</a>
									<?php else:?>
										<a href="<?php echo site_url('admin/gallery_detail?act=unlock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-lock" style="color:red"></i>
												
											</span>
										</a>
									<?php endif;?>
									<a href="#" title="Delete" onclick="btndelete(<?php echo $obj->id?>,'gallery_detail','<?php echo $infoLog->token?>')" data-toggle="modal" data-target="#deleteModal">
                                        <span class="nav-icon">
											<i class="fa fa-fw fa-trash "></i>
										</span>
									</a>
									
								</td>
							</tr>
						<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>