<?php
	$action = $obj?site_url('admin/gallery_detail?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/gallery_detail?act=upd&token='.$infoLog->token);
	$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');
?>
<?php $this->load->view('cms/_layout/datetime'); ?>

<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Chi tiết thông tin khuyến mãi</div>
			</div>
			<div class="page-title-actions">

				<div class="d-inline-block dropdown">
					<a href="<?php echo site_url('admin/gallery_detail')?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-danger">
							Quay lại
						</button>
					</a>
					<a href="<?php echo site_url('admin/gallery_detail?act=upd&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Khuyến Mãi
						</button>
					</a>
				</div>
			</div>    
		</div>
	</div>            
	<div class="main-card mb-3 card">
		<div class="card-body">
			<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Tên khuyến mãi</label>
						<input type="text" class="form-control" name="name" id="validationCustom01" placeholder="Tên khuyến mãi" value="<?php echo $obj?$obj->name:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Phần trăm giảm giá</label>
						<input type="text" class="form-control" name="discount" id="validationCustom01" placeholder="Phần trăm giảm giá (%)" value="<?php echo $obj?$obj->discount:"";?>">
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Ngày bắt đầu</label>
						<input type="text" class="form-control" name="start_date" id="datepicker" placeholder="Ngày bắt đầu" value="<?php echo $obj?date('d-m-Y', $obj->start_date):date("d-m-Y");?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Ngày kết thúc</label>
						<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="Ngày kết thúc" value="<?php echo $obj?date('d-m-Y', $obj->end_date):date("d-m-Y");?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<img id="imgFile_01" class="imgFile img-fluid" alt="Avatar" src="<?php echo $image_01?>" width="200px" height="300px"/>
						<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
					</div>
					<div class="col-md-12 mb-12">
						<label for="exampleText" class="">Mô tả</label>
						<textarea name="description" id="description" class="form-control"><?php echo $obj ? $obj->description : ""; ?></textarea>
						<script>
							var editor = CKEDITOR.replace('description',{
								language:'vi',
								filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

								filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
								
								filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
								
								filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
								
								filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
								
								filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

							});
						</script>
					</div>
				</div>
				<a class="btn btn-default" href="<?php echo site_url('admin/gallery_detail')?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>			
			<?php echo form_close(); ?>
		</div>
	</div>
</div>