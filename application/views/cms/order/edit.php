<?php
	$action = $obj ? site_url('admin/order?act=upd&id=' . $obj->id . "&token=" . $infoLog->token) : site_url('admin/order?act=upd&token=' . $infoLog->token);
?>
<?php $this->load->view('cms/_layout/datetime'); ?>
<div class="app-main__inner">           
	<div class="main-card mb-3 card">
		<div class="card-body">
			<h5 class="card-title">Mua gói</h5>
			<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Tên Khách</label>
						<input type="text" class="form-control" name="name" id="validationCustom01" placeholder="Tên khách hàng" value="<?php echo $obj?$obj->name:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">   
						<label for="validationCustom01">Số điện thoại</label>
						<input type="text" class="form-control" name="phone" id="validationCustom01" placeholder="Số điện thoại" value="<?php echo $obj?$obj->phone:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Gói </label>
						<select name="product_id" id="exampleSelect" class="form-control">
							<?php foreach($products as $product):?>
								<option value="<?php echo $product?$product->id:"";?>">
									<?php echo $product?$product->name:"";?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Số lần </label>
						<input type="text" class="form-control" name="times" id="validationCustom01" placeholder="Số lần" value="<?php echo $obj?$obj->times:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Ngày hết hạn</label>
						<input type="text" class="form-control" name="end_date" id="datepicker" placeholder="Ngày hết hạn" value="<?php echo $obj?date("d-m-Y",$obj->end_date):date("d-m-Y");?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Thanh toán</label>
						<input type="text" class="form-control" name="paid" id="validationCustom01" placeholder="Thanh toán" value="<?php echo $obj?$obj->paid:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Còn lại</label>
						<input type="text" class="form-control" name="debt" id="validationCustom01" placeholder="Còn lại" value="<?php echo $obj?$obj->debt:"";?>" required>
					</div>
					<div class="col-md-6 mb-3">
						<label for="exampleText" class="">Ghi chú</label>
						<textarea name="note" id="exampleText" class="form-control"><?php echo $obj ? $obj->note : ""; ?></textarea>
					</div>
				</div>
				<a class="btn btn-default" href="<?php echo site_url('admin/order'); ?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>			
			<?php echo form_close(); ?>
		</div>
	</div>
</div>