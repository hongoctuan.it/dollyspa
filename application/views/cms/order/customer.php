<div class="app-main__inner">           
	<div class="main-card mb-3 card">
		<div class="card-body">
			<h5 class="card-title">Chi tiết gói đã mua</h5>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Tên Khách</label>
						<input type="text" class="form-control" name="name" id="validationCustom01" placeholder="Tên khách hàng" value="<?php echo $obj?$obj->name:"";?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Số điện thoại</label>
						<input type="text" class="form-control" name="phone" id="validationCustom01" placeholder="Số điện thoại" value="<?php echo $obj?$obj->phone:"";?>" required>
					</div>
					<div class="col-md-12 mb-12">
						<a href="<?php echo site_url('admin/order')?>">
							<button type="button" class="btn-shadow dropdown-toggle btn btn-danger">
								Quay lại
							</button>
						</a>	
					</div>	
				</div>
		</div>
	</div>

	<div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
						<th>#</th>
                            <th>Tên Gói</th>
                            <th>Ngày mua</th>
                            <th class="title_hidden">Tổng tiền</th>
                            <th class="title_hidden">Đã trả</th>
                            <th class="title_hidden">Nợ</th>
                            <th class="title_hidden">Ghi chú</th>
                        </tr>
                        </thead>
                        <tbody>
							<?php if($obj_details):
								foreach($obj_details as $key=>$obj_detail){
							?>
								<tr>
									<th scope="row"><?php echo $key+1?></th>
									<td>
										<a href="<?php echo site_url('admin/order?act=upd_package&id='.$obj_detail->id.'&token='.$infoLog->token)?>">
											<?php echo $obj_detail->name?>
										</a>
									</td>
									<td>
										<?php echo $obj_detail->create_date?>
									</td>
									<td class="content_hidden">
										<?php echo number_format($obj_detail->paid+$obj_detail->debt, 0, ',', '.')?>
									</td>
									<td class="content_hidden">
										<?php echo number_format($obj_detail->paid, 0, ',', '.')?>
									</td>
									<td class="content_hidden">
										<?php echo number_format($obj_detail->debt, 0, ',', '.')?>
									</td>
									<td class="content_hidden">
										<?php echo $obj_detail->note?>
									</td>
								</tr>
							<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>