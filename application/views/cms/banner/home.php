<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Danh sách banner quảng cáo</div>
			</div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    <a href="<?php echo site_url('admin/banner?act=new&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
						
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Banner
						</button>
					</a>
                 
                </div>
            </div>    
        </div>
    </div>            
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
							<th>#</th>
							<th>Hình Ảnh</th>
							<th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($banners):
							foreach($banners as $key=>$obj){
						?>
							<tr>
								<th>
									<?php echo $key+1?>
								</th>
								<td class="banner_img"><img class="img-fluid" src="<?php echo site_url("assets/public/avatar/").$obj->img?>" style="max-width:500px"></td>
								<td>
									<?php if($obj->active==1):?>
										<a href="<?php echo site_url('admin/banner?act=lock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-unlock-alt "></i>
											</span>
										</a>
									<?php else:?>
										<a href="<?php echo site_url('admin/banner?act=unlock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-lock" style="color:red"></i>
												
											</span>
										</a>
									<?php endif;?>
									<a href="#" title="Delete" onclick="btndelete(<?php echo $obj->id?>,'banner','<?php echo $infoLog->token?>')" data-toggle="modal" data-target="#deleteModal">
										<span class="nav-icon">
											<i class="fa fa-fw fa-trash "></i>
										</span>
									</a>
								</td>
							</tr>
						<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>