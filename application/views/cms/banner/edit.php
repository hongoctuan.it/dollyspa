<?php
$action = site_url('admin/banner?act=save&token='.$infoLog->token);
$image_01 = $obj&&$obj->image_01!=""?base_url('assets/public/avatar/'.$obj->image_01):base_url('assets/public/avatar/no-avatar.png');
?>
<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
		<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Chi tiết banner quảng cáo</div>
			</div>
			<div class="page-title-actions">
				<button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
					<i class="fa fa-star"></i>
				</button>
				<div class="d-inline-block dropdown">
				<a href="<?php echo site_url('admin/banner?act=new&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
						
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Banner
						</button>
					</a>
				</div>
			</div>    </div>
	</div>            <div class="main-card mb-3 card">
		<div class="card-body">
			<h5 class="card-title">Bootstrap 4 Form Validation</h5>
			<?php echo form_open_multipart($action, array('autocomplete' => "off", 'id' => "userform")); ?>
				<div class="form-row">
					
					<div class="col-md-6 mb-3">
						<label>Hình Ảnh</label>
						<div>
							<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01 ?>" width="300px" height="200px"/>
							<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<a class="btn btn-default" href="<?php echo site_url('admin/banner'); ?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>			
			<?php echo form_close(); ?>
		</div>
	</div>
</div>