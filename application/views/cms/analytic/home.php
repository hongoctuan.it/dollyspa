<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Số lượng", { role: "style" } ],
        ["Tháng 1", <?php echo $customerMonthByMonth[0]?>, "#d92550"],
        ["Tháng 2", <?php echo $customerMonthByMonth[1]?>, "#3f6ad8 "],
        ["Tháng 3", <?php echo $customerMonthByMonth[2]?>, "#3ac47d"],
        ["Tháng 4", <?php echo $customerMonthByMonth[3]?>, "gold"],
        ["Tháng 5", <?php echo $customerMonthByMonth[4]?>, "pink"],
        ["Tháng 6", <?php echo $customerMonthByMonth[5]?>, "#16aaff"],
        ["Tháng 7", <?php echo $customerMonthByMonth[6]?>, "orange"],
        ["Tháng 8", <?php echo $customerMonthByMonth[7]?>, "violet"],
        ["Tháng 9", <?php echo $customerMonthByMonth[8]?>, "#002200"],
        ["Tháng 10", <?php echo $customerMonthByMonth[9]?>, "purble"],
        ["Tháng 11", <?php echo $customerMonthByMonth[10]?>, "color: #e5e4e2"],
        ["Tháng 12", <?php echo $customerMonthByMonth[11]?>, "brown"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Thống Kê Lượng Khách Trong Năm",
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChartIncome);

      function drawChartIncome() {
        var data = google.visualization.arrayToDataTable([
          ['Doanh thu của tháng hiện tại và tháng trước đó', 'Tổng Doanh Thu', 'Đã thanh toán', 'Còn Nợ'],
          ['Tháng <?php echo date('m')-1; ?>', <?php echo ($incomeByMonth[0]->debt+$incomeByMonth[0]->paid)?>, <?php echo $incomeByMonth[0]->paid?>, <?php echo $incomeByMonth[0]->debt?>],
          ['Tháng <?php echo date('m')-0; ?>', <?php echo ($incomeByMonth[1]->debt+$incomeByMonth[1]->paid)?>, <?php echo $incomeByMonth[1]->paid?>, <?php echo $incomeByMonth[1]->debt?>]
        ]);

        var options = {
          chart: {
            title: 'Doanh thu của tháng hiện tại và tháng trước đó'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
  </script>
<div class="app-main__inner">          
    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Tổng số khách hàng </div>
                        <div class="widget-subheading">Đơn hàng/Đặt chỗ</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo $total_customer_active ?>/<?php echo $total_customer_inactive ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-arielle-smile">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Đã thanh toán</div>
                        <div class="widget-subheading">Tổng số tiền đã thanh toán</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo number_format($total_paid, 0, ',', '.') ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-grow-early">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Chưa thanh toán</div>
                        <div class="widget-subheading">Tổng số tiền chưa thanh toán</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span><?php echo number_format($total_debt, 0, ',', '.') ?></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-6">
            <div class="mb-3 card">
                <div class="card-header-tab card-header-tab-animation card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                        Tổng số khách hàng của tháng
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tabs-eg-77">
                            <div class="card mb-3 widget-chart widget-chart2 text-left w-100">
                                <div class="widget-chat-wrapper-outer">
                                    <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                                        <div id="columnchart_values"></div>

                                    </div>
                                </div>
                            </div>
                            <h6 class="text-muted text-uppercase font-size-md opacity-5 font-weight-normal">Danh sách khách hàng của tháng</h6>
                            <div class="scroll-area-sm">
                                <div class="scrollbar-container">
                                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th>Tên</th>
                                                <th class="text-center">Điện Thoại</th>
                                                <th class="text-center">Tên gói</th>
                                                
                                                <th class="text-center">Nợ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($customerInfoOfMonth):
                                                foreach($customerInfoOfMonth as $key=>$obj){
                                            ?>
                                                <tr>
                                                    <td class="text-center text-muted"><?php echo $key+1?></td>
                                                    <td>
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left flex2">
                                                                    <div class="widget-heading"><?php echo $obj->customerName?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><?php echo $obj->phone?></td>
                                                    <td class="text-center">
                                                        <div class="badge badge-warning"><?php echo $obj->productName?></div>
                                                    </td>
                                                    
                                                    <td class="text-center">
                                                        <div class="widget-heading"><?php echo number_format($obj->debt, 0, ',', '.')?></div>
                                                    </td>
                                                </tr>
                                            <?php } endif;?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6">
            <div class="mb-3 card">
                <div class="card-header-tab card-header">
                    <div class="card-header-title">
                        <i class="header-icon lnr-rocket icon-gradient bg-tempting-azure"> </i>
                        Doanh thu 2 tháng gần nhất
                    </div>
                    
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="tab-eg-55">
                        <div class="widget-chart p-3">
                            <div style="height: 350px">
                                <div id="columnchart_material" style=" height: 500px;"></div>
                            </div>
                            <div class="widget-chart-content text-center mt-5">
                                <div class="widget-description mt-0 text-warning">
                                    <i class="fa fa-arrow-left"></i>
                                </div>
                            </div>
                        </div>
                        <div class="pt-2 card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-outer">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-outer">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-outer">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="widget-content">
                                        <div class="widget-content-outer">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Số khách hết gói trong 30 ngày</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-warning"><?php echo $totalExpireCustomer?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content">
                <div class="widget-content-outer">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-heading">Tổng số khách hàng còn gói</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-danger"><?php echo $totalActiveCustomer?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-header">Khách hàng đã hết gói trong 30 ngày
                    <div class="btn-actions-pane-right">
                        <div role="group" class="btn-group-sm btn-group">
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Tên</th>
                            <th class="text-center">Điện Thoại</th>
                            <th class="text-center">Gói đã dùng</th>
                            <th class="text-center">Ngày hết hạn</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($expireCustomer):
							foreach($expireCustomer as $key=>$obj){
						?>
                            <tr>
                                <td class="text-center text-muted"><?php echo $key+1?></td>
                                <td>
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left flex2">
                                                <div class="widget-heading"><?php echo $obj->customerName?></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center"><?php echo $obj->phone?></td>
                                <td class="text-center">
                                    <div class="badge badge-warning"><?php echo $obj->productName?></div>
                                </td>
                                <td class="text-center">
                                    <div class="widget-heading"><?php echo date('d-m-Y',$obj->expireTime)?></div>
                                </td>
                            </tr>
                        <?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
