<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V16</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="statics/cms/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="statics/cms/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="statics/cms/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="statics/cms/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="statics/cms/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="statics/cms/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="statics/cms/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="statics/cms/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="statics/cms/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="statics/cms/css/util.css">
	<link rel="stylesheet" type="text/css" href="statics/cms/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('statics/cms/images/login_background.jpeg');">
		<div class="wrap-login100 p-t-30 p-b-50">
				<?php if(isset($_SESSION['system_msg'])){
					echo $_SESSION['system_msg'];unset($_SESSION['system_msg']);
				}?>
				<form class="login100-form validate-form p-b-33 p-t-5" action="<?php echo site_url('login')?>" method="post">

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Tài khoản" required name="username">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="Mật Khẩu" required name="password">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>

					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn" type="submit">
							Gửi
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="statics/cms/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="statics/cms/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="statics/cms/vendor/bootstrap/js/popper.js"></script>
	<script src="statics/cms/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="statics/cms/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="statics/cms/vendor/daterangepicker/moment.min.js"></script>
	<script src="statics/cms/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="statics/cms/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="statics/cms/js/main.js"></script>

</body>
</html>