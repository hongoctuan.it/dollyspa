<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Danh sách gói đã mua</div>
			</div>
            <div class="page-title-actions">

                <div class="d-inline-block dropdown">
                    <a href="<?php echo site_url('admin/order?act=upd&&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
						
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Gói
						</button>
					</a>
                 
                </div>
            </div>    
        </div>
    </div>            
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">Table striped</h5>
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên</th>
                            <th>Điện Thoại</th>
                            <th>Ngày mua</th>
							<th>Gói</th>
                            <th class="title_hidden">Đã trả</th>
                            <th class="title_hidden">Nợ</th>
                            <th class="title_hidden">Ghi chú</th>
                            <th class="title_hidden">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if($order):
							$dem=1;
							foreach($order as $obj){
						?>
							<tr>
								
								<?php if($obj->flag==true):?>
									<th scope="row" <?php echo $obj->flag==true? 'rowspan="'.$obj->rowspan.'"':"" ?>><?php echo $dem++?></th>
									<td <?php echo $obj->flag==true? 'rowspan="'.$obj->rowspan.'"':"" ?>>
										<a href="<?php echo site_url('admin/order?act=upd_customer&id='.$obj->customer_id.'&token='.$infoLog->token)?>">
											<?php echo $obj->name?>
										</a>
									</td>
								<?php endif; ?>
                                <td>
									<?php echo $obj->phone?>
								</td>
                                <td>
									<?php echo (date("d-m-Y",$obj->create_date))?>
								</td>
								<td>
									<a href="<?php echo site_url('admin/order?act=upd_package&id='.$obj->id.'&token='.$infoLog->token)?>">
										<?php echo $obj->product_name?>
									</a>
								</td>
                                <td class="content_hidden">
									<?php echo $obj->paid?>
								</td>
                                <td class="content_hidden">
									<?php echo $obj->debt?>
								</td>
                                <td class="content_hidden">
									<?php echo $obj->note?>
								</td>
                                
								<td class="content_hidden">
									<?php if($obj->active==1):?>
										<a href="<?php echo site_url('admin/search?act=lock&id='.$obj->id."&control=order&value=".$value."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-unlock-alt "></i>
											</span>
										</a>
									<?php else:?>
										<a href="<?php echo site_url('admin/search?act=unlock&id='.$obj->id."&control=order&value=".$value."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-lock" style="color:red"></i>
												
											</span>
										</a>
									<?php endif;?>
								</td>
							</tr>
						<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>