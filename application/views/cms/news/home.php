
<div class="app-main__inner">          
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5>Danh sách thông tin giới thiệu</h5>
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
						<th>#</th>
								<th>Tiêu đề</th>
								<th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($news):
							foreach($news as $key=>$obj){
						?>
							<tr>
								<th scope="row"><?php echo $key+1?></th>
								<td>
									<a href="<?php echo site_url('admin/news?act=upd&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<?php echo $obj->name?>
									</a>
								</td>
								<td>
								<a href="<?php echo site_url('admin/news?act=upd&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
										<span class="nav-icon">
											<i class="fa fa-fw fa-edit "></i>
										</span>
									</a>
								</td>
							</tr>
						<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>