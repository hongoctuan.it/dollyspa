
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
				<div class="page-title-icon">
                    <img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Thông Tin Spa</div>
			</div>
            <div class="page-title-actions">
                <div class="d-inline-block dropdown">
                    <a href="<?php echo site_url('admin/companyinfo?act=upd&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
						
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Sửa Thông Tin
						</button>
					</a>
                 
                </div>
            </div>    
        </div>
    </div>            
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
							<th>#</th>
							<th>Thông Tin</th>
							<th class="title_hidden">Giá Trị</th>
							<th class="title_hidden" style="width:15%">Lần Sửa Cuối</th>
                        </thead>
                        <tbody>
                        <?php if(isset($infos)):
							foreach($infos as $key=>$obj){
						?>
							<tr>
								<th scope="row"><?php echo $obj->id?></th>
								<td><?php echo ucwords($obj->info)?></td>
								<td class="content_hidden"><?php echo $obj->value?></td>
								<td class="content_hidden"><?php echo $obj->last_updated?></td>
							</tr>
						<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>