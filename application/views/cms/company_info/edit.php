<?php
$action = $obj?site_url('admin/companyinfo?act=upd&token='.$infoLog->token):site_url('admin/companyinfo?act=upd&token='.$infoLog->token);
?>

<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Chi tiết thông Tin Spa</div>
			</div>
		</div>
	</div>            
	<div class="main-card mb-3 card">
		<div class="card-body">
			
			<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
				<div class="form-row">
					<?php foreach($infos as $obj):?>
						<?php if($obj->info=="time"):?>
							<div class="col-md-4 mb-3">
								<div class="form-group required">
									<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
									<textarea id="content" class="form-control" name="<?php echo $obj?$obj->info:'';?>" required id="content" ><?php echo $obj?$obj->value:"";?></textarea>

								</div>
								<script>
									var editor = CKEDITOR.replace('content',{
										language:'vi', 
										height: '100px',
										filebrowserBrowseUrl :'<?php echo base_url()."filemanager/ckfinder/ckfinder.html"?>',

										filebrowserImageBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Images"?>',
										
										filebrowserFlashBrowseUrl : '<?php echo base_url()."filemanager/ckfinder/ckfinder.html?type=Flash"?>',
										
										filebrowserUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>',
										
										filebrowserImageUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images"?>',
										
										filebrowserFlashUploadUrl : '<?php echo base_url()."filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"?>',

									});
                        		</script>
							</div>
						<?php elseif($obj->info=="intro"):?>
							<div class="col-md-4 mb-3">
								<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
								<textarea class="form-control" name="<?php echo $obj?$obj->info:'';?>" required rows="10" ><?php echo $obj?$obj->value:"";?></textarea>
							</div>
						<?php else:?>
							<div class="col-md-3 mb-3">
								<div class="form-group required">
									<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
									<input type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required value="<?php echo $obj?$obj->value:" ";?>"/>
								</div>
							</div>
						<?php endif;?>
					<?php endforeach;?>
					
				</div>

				<a class="btn btn-default" href="<?php echo site_url('admin/companyinfo'); ?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>			
			<?php echo form_close(); ?>
		</div>
	</div>
</div>