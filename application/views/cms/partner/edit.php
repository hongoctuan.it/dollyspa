<?php
if(isset($id)){
	$action = site_url('admin/partner?act=upd&id='.$id.'&token='.$infoLog->token);
}else{
	$action = site_url('admin/partner?act=new&token='.$infoLog->token);
}
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Nguyên Quân / Nhà phân phối
				</h6>
				<h3 class="dashhead-title">Quản lý nhà phân phối</h3>
			</div>

			<div class="dashhead-toolbar">
				<div class="dashhead-toolbar-item">
					Nhà phân phối / Quản lý nhà phân phối
				</div>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tên của hàng</label>
									<input type="text" class="form-control" name="Partner[name]" id="name" required value="<?php if(isset($obj->name)) echo $obj->name;?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Slug</label>
									<input type="text" class="form-control" name="Partner[slug]" id="slug" required value="<?php if(isset($obj->slug)) echo $obj->slug;?>"/>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Miền</label>
									<select name="Partner[area]">
										<option value="0" <?php if(isset($obj->area) && $obj->area == 0)  echo "selected";?>>Miền Nam</option>
										<option value="1" <?php if(isset($obj->area) && $obj->area == 1)  echo "selected";?>>Miền Trung</option>
										<option value="2" <?php if(isset($obj->area) && $obj->area == 2)  echo "selected";?>>Miền Bắc</option>
									</select>								
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Địa chỉ</label>
									<input id="content" class="form-control" name="Partner[address]" required id="address" value="<?php if(isset($obj->address)) echo $obj->address;?>"/>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Điện thoại</label>
									<input id="content" class="form-control" name="Partner[phone]" required id="phone" value="<?php if(isset($obj->phone)) echo $obj->phone;?>"/>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group required">
									<label class="control-label">Google Map Link</label>
									<p><small>Để lấy link: vào google map, chọn địa điểm rồi bấm chia sẻ. Trong phần Nhúng Bản Đồ copy link trong src, bỏ phần phần 'https://' và chép vào form dưới</small></p>
									<input id="content" type="text" class="form-control" name="Partner[map_src]" required id="map_src" value="<?php if(isset($obj->map_src)) echo $obj->map_src;?>"/>
								</div>
							</div>
							
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/introduce');?>">Quay lại</a>
								<button type="reset" class="btn btn-warning">Huỷ</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->