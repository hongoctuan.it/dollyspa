<div class="app-main__inner">       
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="mb-0 table table-striped" style="display:block; overflow-x: auto;overflow: scroll;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên Khách</th>
                            <th>Điện Thoại</th>
                            <th>Ngày đến</th>
                            <th class="title_hidden">Thời gian đến</th>
                            <th class="title_hidden">Số người</th>
                            <th>Dịch vụ</th>
                            <th class="title_hidden">Ghi chú</th>
                            <th>Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($booking):
							foreach($booking as $key=>$obj){
						?>
							<tr>
								<th scope="row"><?php echo $obj->id?></th>
								<td style="width:20%">
										<?php echo $obj->name?>
								</td>
                                <td>
										<?php echo $obj->phone?>
								</td>
                                <td>
										<?php echo date('d-m-Y',$obj->date)?>
								</td>
                                <td class="content_hidden">
										<?php echo $obj->time?>
								</td>
                                <td class="content_hidden">
										<?php echo $obj->number?>
								</td>
                                <td >
										<?php echo $obj->product?>
								</td>
                                <td class="content_hidden">
										<?php echo $obj->note?>
								</td>
								
								<?php 
                                $time = date('c', strtotime('-1 days'));
                                $time = new DateTime($time);
                                $time = $time->getTimestamp();
                                if($obj->date < $time):?>
									<td class="badge badge-warning">
										Hết hạn
									</td>
								<?php else:?>
									<td class="badge badge-info">
										Còn hạn
									</td>
								<?php endif;?>
								
									
									
								
							</tr>
						<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>