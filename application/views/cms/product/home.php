<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Danh sách sản phẩm</div>
			</div>
		<div class="page-title-actions">
                <div class="d-inline-block dropdown">
					<a href="<?php echo site_url('admin/category')?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-danger">
							Quay lại
						</button>
					</a>
                    <a href="<?php echo site_url('admin/product?act=upd&cat='.$category.'&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Sản Phẩm
						</button>
					</a>
                 
                </div>
            </div>    
        </div>
    </div>            
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="mb-0 table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên Sản Phẩm</th>
                            <th>Giá</th>
                            <th class="title_hidden">Thời gian</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($product):
							foreach($product as $key=>$obj){
						?>
							<tr>
								<th scope="row"><?php echo $obj->id?></th>
								<td style="width:30%">
									<?php echo $obj->name?>
								</td>
								<td style="width:10%">
									<?php echo number_format($obj->price, 0, ',', '.')?>
								</td>
								<td class="content_hidden"><?php echo $obj->time?></td>
								<td>
                                    <a href="<?php echo site_url('admin/product?act=upd&id='.$obj->id."&cat=".$obj->category_id."&token=".$infoLog->token);?>" title="Edit">

										<span class="nav-icon">
											<i class="fa fa-fw fa-edit "></i>
										</span>
									</a>
									<?php if($obj->active==1):?>
										<a href="<?php echo site_url('admin/product?act=lock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-unlock-alt "></i>
											</span>
										</a>
									<?php else:?>
										<a href="<?php echo site_url('admin/product?act=unlock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit">
											<span class="nav-icon">
												<i class="fa fa-fw fa-lock" style="color:red"></i>
												
											</span>
										</a>
									<?php endif;?>
									<a href="#" title="Delete" onclick="btndelete(<?php echo $obj->id?>,'product','<?php echo $infoLog->token?>')" data-toggle="modal" data-target="#deleteModal">
                                        <span class="nav-icon">
											<i class="fa fa-fw fa-trash "></i>
										</span>
									</a>
									
								</td>
							</tr>
						<?php } endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>