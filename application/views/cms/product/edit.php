<?php
$action = $obj ? site_url('admin/product?act=upd&id=' . $obj->id . "&token=" . $infoLog->token) : site_url('admin/product?act=upd&token=' . $infoLog->token);
$image_01 = $obj && $obj->img1 != "" ? base_url('assets/public/avatar/' . $obj->img1) : base_url('assets/public/avatar/no-avatar.png');
$image_02 = $obj && $obj->img2 != "" ? base_url('assets/public/avatar/' . $obj->img2) : base_url('assets/public/avatar/no-avatar.png');
$image_03 = $obj && $obj->img3 != "" ? base_url('assets/public/avatar/' . $obj->img3) : base_url('assets/public/avatar/no-avatar.png');
$image_04 = $obj && $obj->img4 != "" ? base_url('assets/public/avatar/' . $obj->img4) : base_url('assets/public/avatar/no-avatar.png');
?>
<div class="app-main__inner" style="flex:none">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon">
					<img src="<?php echo site_url('statics/default/img/logo_small.png');?>" width="100%"/>
				</div>
				<div>Chi tiết sản phẩm</div>
			</div>
			<div class="page-title-actions">
					<a href="<?php echo site_url('admin/product?act=child_list&id='.$cat.'&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-danger">
							Quay lại
						</button>
					</a>
					<a href="<?php echo site_url('admin/product?act=upd&cat='.$cat.'&token='.$infoLog->token)?>">
						<button type="button" class="btn-shadow dropdown-toggle btn btn-info">
							<span class="btn-icon-wrapper pr-2 opacity-7">
								<i class="fa fa-business-time fa-w-20"></i>
							</span>
							Thêm Gói
						</button>
					</a>
				</div>
			</div>    </div>
	</div>            <div class="main-card mb-3 card">
		<div class="card-body">
			<?php echo form_open_multipart($action, array('autocomplete' => "off", 'id' => "userform")); ?>
				<input type="hidden" name="category_id" value="<?php echo $cat ? $cat : ""; ?>"/>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Tên Dịch Vụ</label>
						<input type="text" class="form-control" name="name" id="validationCustom01" placeholder="Tên dịch vụ chăm sóc" value="<?php echo $obj ? $obj->name : ""; ?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom02">Giá Dịch Vụ</label>
						<input type="number" class="form-control" name="price" id="validationCustom02" placeholder="Giá dịch vụ" value="<?php echo $obj ? $obj->price : ""; ?>" required>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom02">Thời gian</label>
						<input type="text" class="form-control" name="time" id="validationCustom02" placeholder="Thời gian" value="<?php echo $obj ? $obj->time : ""; ?>" required>
					</div>
				</div>
				<a class="btn btn-default" href="<?php echo site_url('admin/product?act=child_list&id='.$cat.'&token='.$infoLog->token)?>">Quay lại</a>
				<button type="reset" class="btn btn-warning">Huỷ</button>
				<button type="submit" id="formSubmit" class="btn btn-primary">Lưu Lại</button>			
				<?php echo form_close(); ?>
		</div>
	</div>
</div>