<section class="shops-banner">

    <div class="shops-banner-wrap text-center">
        <h1 class="shops-banner-title text-white text-center">
            Đối tác của chúng tôi
        </h1>
    </div>
    </div>

</section>
<section class="product-details mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 pt-1">
                <ul class="nav nav-tabs text-center justify-content-center bg-light stores-menu m-auto" id="myTab" role="tablist">
                    <li class="nav-item mx-3 my-2">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#product-details-desc" role="tab" aria-controls="product-details-desc" aria-selected="true">Đối tác miền
                            Bắc</a>
                    </li>
                    <li class="nav-item mx-3 my-2">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#product-details-more-info" role="tab" aria-controls="product-details-more-info" aria-selected="false">Đối tác miền
                            Trung</a>
                    </li>
                    <li class="nav-item mx-3 my-2">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#product-details-brand-info" role="tab" aria-controls="product-details-brand-info" aria-selected="false">Đối tác miền
                            Nam</a>
                    </li>

                </ul>
            </div>

            <div class="col-md-12">
                <div class="tab-content text-center" id="myTabContent">


                    <div class="tab-pane fade show active" id="product-details-desc" role="tabpanel" aria-labelledby="product-details-desc-tab">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row shops-mien">
                                    <div class="col-md-12 shops-list">
                                        <div class="shops-list-title-section">
                                            <h3 class=shops-list-title>Miền Bắc</h3>
                                        </div>
                                        <?php foreach ($partner_bac as $item) : ?>
                                            <div class="shops-wrap shops-show-default">
                                                <h3 class="shops-title text-center mt-2"><?php echo $item->name; ?></h3>
                                                <p class="shops-address">Địa chỉ: <?php echo $item->address; ?>
                                                    <br>Số ĐT: <?php echo $item->phone; ?></p>
                                                <input type=hidden value="<?php echo $item->map_src; ?>" class="map_src">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 google-map-iframe-wrap">
                                <iframe class="google-map-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62694.93338363085!2d106.71814491147437!3d10.854608470658878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d85e042bf04b%3A0xbb26baec1664394d!2zVGjhu6cgxJDhu6ljLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1555423397586!5m2!1svi!2s" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="product-details-more-info" role="tabpanel" aria-labelledby="product-details-more-info-tab">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row shops-mien">
                                    <div class="col-md-12 shops-list">
                                        <div class="shops-list-title-section">
                                            <h3 class=shops-list-title>Miền Trung</h3>
                                        </div>
                                        <?php foreach ($partner_trung as $item) : ?>
                                            <div class="shops-wrap shops-show-default">
                                                <h3 class="shops-title text-center mt-2"><?php echo $item->name; ?></h3>
                                                <p class="shops-address">Địa chỉ: <?php echo $item->address; ?>
                                                    <br>Số ĐT: <?php echo $item->phone; ?></p>
                                                <input type=hidden value="<?php echo $item->map_src; ?>" class="map_src">
                                            </div>

                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 google-map-iframe-wrap">
                                <iframe class="google-map-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62694.93338363085!2d106.71814491147437!3d10.854608470658878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d85e042bf04b%3A0xbb26baec1664394d!2zVGjhu6cgxJDhu6ljLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1555423397586!5m2!1svi!2s" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="product-details-brand-info" role="tabpanel" aria-labelledby="product-details-brand-info-tab">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row shops-mien">
                                    <div class="col-md-12 shops-list">
                                        <div class="shops-list-title-section">
                                            <h3 class=shops-list-title>Miền Nam</h3>
                                        </div>
                                        <?php foreach ($partner_nam as $item) : ?>
                                            <div class="shops-wrap shops-show-default">
                                                <h3 class="shops-title text-center mt-2"><?php echo $item->name; ?></h3>
                                                <p class="shops-address">Địa chỉ: <?php echo $item->address; ?>
                                                    <br>Số ĐT: <?php echo $item->phone; ?></p>
                                                <input type=hidden value="<?php echo $item->map_src; ?>" class="map_src">
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 google-map-iframe-wrap">
                                <iframe class="google-map-iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62694.93338363085!2d106.71814491147437!3d10.854608470658878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d85e042bf04b%3A0xbb26baec1664394d!2zVGjhu6cgxJDhu6ljLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1555423397586!5m2!1svi!2s" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>