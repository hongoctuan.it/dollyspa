<div class="service">
            <div class="section-title-wrap-v2 mb-4">
                <h1 class="text-center section-title-v2 m-auto px-3">DỊCH VỤ</h1>
            </div>
            <div class="category-wrap-box">
            <?php $index = 0; ?>
            <?php foreach ($categories as $key => $category) : ?>
                    <?php if (!empty($category->name)) : ?>
                        <?php if ($index==0) : ?>
                            <button class="tablink" onclick="openPage('<?php echo $category->slug?>', this, '#5b7034')" id="defaultOpen"><?php echo $category->name?></button>
                        <?php else:?>
                            <button class="tablink" onclick="openPage('<?php echo $category->slug?>', this, '#5b7034')" ><?php echo $category->name?></button>
                        <?php endif;?>
                        <?php $index++; ?>
                    <?php endif; ?>
                   
            <?php endforeach; ?>
            <?php foreach ($categories as $key => $category) : ?>
                    <div id="<?php echo $category->slug?>" class="tabcontent">
                    <?php foreach ($category->products as $product) : ?>
                        <?php if (!empty($product)) : ?>
                            <div class="product">
                                <p class="name"><?php echo $product->name ?></p>
                                <p class="separator_dots"></p>
                                <p class="price"><?php echo number_format($product->price, 0, ',', '.') ?>/<?php echo $product->time ?>'</p>
                            </div>
                        <?php endif; ?>        
                    <?php endforeach; ?>
                    </div>
            <?php endforeach; ?>
<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
    tablinks[i].style.color = color;
  }
  document.getElementById(pageName).style.display = "flow-root";
  elmnt.style.backgroundColor = color;
  elmnt.style.color = "#eae3d3";
}

document.getElementById("defaultOpen").click();
</script>
            </div>
</div>
