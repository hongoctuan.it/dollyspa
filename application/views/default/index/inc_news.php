    <div class="row section-title-wrap-v2 mb-4">
        <h1 class="text-center section-title-v2 m-auto px-3">THIÊN ĐƯỜNG THƯ GIÃN</h1>
    </div>

    <div class="row section-content-wrap-2">
        <?php if ($news) : ?>
            <?php $index = 0; ?>
            <?php foreach ($news as $news) : ?>
                <?php
                $index++;
                if ($index > 5) {
                    break;
                }
                ?>
                    <div class='featured-product-item-wrap'>
                        <figure class="figure featured-products-figure">
                                <img class="img-fluid" src="assets/public/avatar/<?php echo $news->img ?>" alt="<?php echo $news->name ?>" style="height:166px">
                                <figcaption class=" m-auto">
                                <p class="mt-2 title_news"><?php echo $news->name ?></p>
                                <div class='index-product-price'><?php echo $news->description ?></div>
                            </figcaption>
                        </figure>
                    </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
