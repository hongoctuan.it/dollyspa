<div class="">
    <div class="row section-title-wrap-v2 mb-4">
        <div class="section-title-line"></div>
        <h1 class="text-center section-title-v2 m-auto px-3">Khuyến Mãi <span style="white-space:nowrap">Dolly Spa</span></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-12 promotion_block">
        <a href="<?php echo site_url("khuyen-mai/".$promotions[0]->id."/".$promotions[0]->slug); ?>">
            <img  src="<?php echo base_url(); ?>assets/public/avatar/<?php echo $promotions[0]->img; ?>" />
            <p class="feature-item-title mt-0"><?php echo $promotions[0]->name; ?></p>
        </a>
    </div>
    <div class="col-lg-6 col-md-12 promotion_block">
        <a href="<?php echo site_url("khuyen-mai/".$promotions[1]->id."/".$promotions[1]->slug); ?>">
            <img src="<?php echo base_url(); ?>assets/public/avatar/<?php echo $promotions[1]->img; ?>" />
            <p class="feature-item-title mt-0"><?php echo $promotions[1]->name; ?></p>
        </a>
    </div>
</div>
<div class="promotionbutton">
    <a href="<?php echo site_url("khuyen-mai");?>"><button class="btn">Danh sách khuyến mãi</button></a>
</div>