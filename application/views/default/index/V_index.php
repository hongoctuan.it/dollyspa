<script src="<?php echo site_url('statics/default/jquery/dist/jquery.min.js'); ?>"></script>
<section class="home-banner-slide-v2 mb-3 md-3">
        <div class="container-fluid">
                <div class="owl-carousel home-banner-slide-carousel-v2">
                    <?php if ($banners) : ?>
                        <?php foreach ($banners as $banner) : ?>
                            <div class="row justify-content-center banner-wrap">
                                <div class="col-12 banner_img">
                                    <a class="banner-wrap-a banner-wrap-item" href='<?php echo $banner->link ? $banner->link : '#'; ?>'                                >
                                        <img class="banner-image img-fluid owl-lazy" data-src="<?php echo base_url(); ?>assets/public/avatar/<?php echo $banner ? $banner->img : ''; ?>" alt="Banner Image">
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
        </div>
    </section>
    <section class="home-featured-products-v2 pt-4 mt-4">
        <?php include 'inc_news.php';?>
    </section>

    <section class="home-category-2">
        <?php include 'inc_service.php';?>
    </section>

    <section class="home-featured-promotion pt-4 my-5">
        <?php include 'inc_promotion.php';?>
    </section>
