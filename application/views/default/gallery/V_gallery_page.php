<div class="banner">
    <h3 style="font-weight:bold"><?php echo $obj->name ?></h3>
</div>
<section class="category-product-list-v2">
    <div class="bg-white service_content" style="padding-top:15px">
        <div class="category-product-list-filter">
            <div class="row" style="margin:auto">
                <div class="col-lg-8 col-md-12" style="text-align:left">
                    <h5 style="color:#FF8F1C; font-weight:bold">Từ <?php echo date('d-m-Y',$obj->start_date)?> - Đến <?php echo date('d-m-Y',$obj->end_date)?></h5>
                    <?php echo $obj->description ?>
                    <img class="event_image" style="height:auto !important" src="<?php echo base_url(); ?>assets/public/avatar/<?php echo $obj->img;?>"/>
                </div>
                <div class="col-lg-4 col-md-12" style="text-align:left">
                    <h5 style="color:#FF8F1C; font-weight:bold">Danh sách khuyến mãi</h5>
                    <?php foreach($promotions as $promotion):?>
                        <a href="<?php echo site_url("khuyen-mai/".$promotion->id."/".$promotion->slug); ?>"><div class="row" style="margin-bottom:10px">
                            <img class="col-lg-4 col-md-4 promotionrefimage" src="<?php echo base_url(); ?>assets/public/avatar/<?php echo $promotion->img;?>"/>
                            <div class="col-lg-8 col-md-8">
                                <p style="font-weight:bold;margin:0px"><?php echo $promotion->name?></p>
                                <p style="margin:0px">Đến ngày: <?php echo date('d-m-Y',$promotion->end_date)?></p>
                            </div>
                        </div>
                        </a>
                    <?php endforeach?>
                </div>
            </div>
        </div>
    </div>
</section>
