<!-- Footer -->
<footer class="footer-distributed2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-3">
                    <h3 class='mb-3'>Giới Thiệu
                    </h3>
                    <div class="footer-address">
                        <?php echo $company_info['intro']->value ?>
                    </div>
                </div>
                <div class="col-md-12 col-lg-3">
                    <h3 class='mb-3 time_title'>Giờ mở
                    </h3>

                    <div class='mb-3 time_content'><?php echo $company_info['time']->value ?></div>
                </div>
                <div class="col-md-12 col-lg-3">
                    <h3 class='mb-3'>Liên Hệ
                    </h3>
                    <h1 style="font-size: x-large;">Dolly Spa Quận Tân Phú</h1>
                    <p><i class="fas fa-map-marker-alt"></i>&nbsp;<?php echo $company_info['address']->value ?></p>
                    <p><i class="fas fa-phone-alt"></i>&nbsp;<?php echo $company_info['phone']->value ?></p>
                    <p><i class="far fa-envelope"></i></i>&nbsp;<?php echo $company_info['email']->value ?></p>
                </div>
                <div class="col-md-12 col-lg-3">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.221991390627!2d106.63282721538808!3d10.794302592309409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529549e285077%3A0x1e16f1e0aa36917!2zNDViIETDom4gVOG7mWMsIFTDom4gVGjhuqFuaCwgVMOibiBQaMO6LCBUaMOgbmggcGjhu5EgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1623382565484!5m2!1svi!2s" width="300" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    <!-- <img src="<?php echo base_url(); ?>assets/public/avatar/<?php echo $company_info['map']->value; ?>" width="300px" height="200px" style="border-radius:10px"/> -->
                </div>
            </div>
        </div>
        <!-- <script src="https://sp.zalo.me/plugins/sdk.js"></script> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
        <script src="<?php echo base_url('statics/default/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo site_url('statics/default/owl/dist/owl.carousel.min.js'); ?>"></script>
        <!-- <script src="<?php echo site_url('statics/default/animejs/anime.min.js'); ?>"></script> -->
        <!-- <script src="<?php echo site_url('statics/default/fancybox-master/dist/jquery.fancybox.min.js'); ?>"></script> -->
        <?php if ($this->uri->segment(1) != "") : ?>
            <script src="<?php echo site_url('statics/default/masonry/imagesloaded.pkgd.js'); ?>"></script>
            <script src="<?php echo site_url('statics/default/masonry/isotope.pkgd.js'); ?>"></script>
            <script src="<?php echo site_url('statics/default/swiper/dist/js/swiper.min.js'); ?>"></script>
        <?php endif; ?>
        <script src="<?php echo site_url('statics/default/swiper/dist/js/swiper.min.js'); ?>"></script>
        <?php $this->load->view('default/_layout/scripts2'); ?>
        <script>
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip();
            });
                $('body').on('click', ".load-more", function() {
                    $('.load-more').fadeOut();
                    var category = <?php echo isset($category_id) ? $category_id : 0; ?>;
                    var page = $('.current-page').val();
                    var stopped = $('.stopped').val();
                    var style = '<?php echo isset($_GET['style']) ? $_GET['style'] : "grid"; ?>';
                    var order = '<?php echo isset($_GET['order']) ? $_GET['order'] : false; ?>';
                    var by = '<?php echo isset($_GET['by']) ? $_GET['by'] : false; ?>';
                    $element = $('.product-list-content');
                    if (stopped == 1) {
                        return false;
                    }
                    page++;
                    $.ajax({
                        method: 'POST',
                        dataType: 'text',
                        url: '<?php echo site_url("/category/ajax/getpage"); ?>',
                        data: {
                            page: page,
                            category: category,
                            style: style,
                            order: order,
                            by: by
                        },
                        success: function(result) {
                            if (result) {
                                $('.current-page').val(page);
                                $element.append($(result).hide().fadeIn(400));
                                $('.load-more').fadeIn(400);
                                setTimeout(
                                    function() {
                                        $('.lds-spinner').fadeOut();
                                        setTimeout(
                                            function() {
                                                $('.product-image').delay().addClass('show');
                                                $('.featured-products-img').delay().addClass('show');
                                                $('.category-product-item-img img').addClass('show');
                                                $('.category-sidebar-hot-products-item img').addClass('show');
                                                $('.banner-image').addClass('show');
                                            }, 200);
                                    }, 500);
                            } else {
                                $('.stopped').val(1);
                                $element.append($('<div class=col-12><p class=text-center><b>không còn sản phẩm</b></p></div>').hide().fadeIn(500));
                                $('.load-more').fadeOut(500);
                            }

                        }
                    })
                });

            <?php if ($this->uri->segment(1) == "thu-vien") : ?>
                $(document).ready(function() {
                    $('body').on('click', ".gallery-see-more", function() {
                        $('.gallery-see-more').fadeOut(500);
                        var category = <?php echo isset($gallery_data) ? $gallery_data->id : 0; ?>;
                        var page = $('.current-page').val();
                        var stopped = $('.stopped').val();
                        $element = $('.grid');
                        if (stopped == 1) {
                            return false;
                        }
                        page++;
                        $.ajax({
                            method: 'POST',
                            dataType: 'text',
                            url: '<?php echo site_url("/gallery/ajax/getpage"); ?>',
                            data: {
                                page: page,
                                category: category
                            },
                            success: function(result) {
                                if (result) {
                                    $('.current-page').val(page);
                                    $element.isotope('insert', $(result).hide().fadeIn(500));
                                    $('.grid').imagesLoaded().progress(function() {
                                        $('.grid').isotope('layout');
                                    });
                                    $('.gallery-see-more').delay(1000).fadeIn(500);
                                } else {
                                    $('.stopped').val(1);
                                    $element.after($('<div class=col-12><p class=text-center><b>không còn hình ảnh</b></p></div>').hide().fadeIn(500));
                                    $('.gallery-see-more').fadeOut(500);
                                }

                            }
                        });
                    });
                });
            <?php endif; ?>


            $(document).ready(function() {
                $('body').on('click', ".partner-registry-btn", function() {
                    var representator = $('.partner-representator').val();
                    var company = $('.partner-company').val();
                    var phone = $('.partner-phone').val();
                    var email = $('.partner-email').val();
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (representator && company && phone && email) {
                        if (re.test(email)) {
                            $.ajax({
                                method: 'POST',
                                dataType: 'text',
                                url: '<?php echo site_url("main/partner_registry"); ?>',
                                data: {
                                    representator: representator,
                                    company: company,
                                    phone: phone,
                                    email: email
                                },
                                success: function(result) {
                                    console.log('result');
                                    if (result == 1) {
                                        if ($('.error-msg')) {
                                            $('.error-msg').fadeOut();
                                            $('.register-partner').slideUp(500);
                                            $('.modal-content').append('<p class=text-center><b>Thông tin đã được gửi thành công</b></p>');
                                        } else {
                                            $('.register-partner').slideUp(500);
                                            $('.modal-content').append('<p class=text-center><b>Thông tin đã được gửi thành công</b></p>');
                                        }
                                    } else {
                                        $('.modal-content').append('<p class=text-center style="color:red"><b>Đã có lỗi xảy ra</b></p>');
                                    }
                                }
                            });
                            if ($('.error-msg')) {
                                $('.error-msg').fadeOut();
                                $('.register-partner').slideUp(500);
                                $('.modal-content').append('<p class=text-center><b>Thông tin đã được gửi thành công</b></p>');
                            } else {
                                $('.register-partner').slideUp(500);
                                $('.modal-content').append('<p class=text-center><b>Thông tin đã được gửi thành công</b></p>');
                            }
                        } else {
                            if ($('.error-msg')) {
                                $('.error-msg').fadeOut();
                                $('.modal-content').delay(2000).fadeIn().append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đúng email</b></p>');
                                $('.error-msg').delay(4000).fadeOut();
                            } else {
                                $('.modal-content').append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đúng email</b></p>');
                                $('.error-msg').delay(4000).fadeOut();
                            }
                        }
                    } else {
                        if ($('.error-msg')) {
                            $('.error-msg').fadeOut();
                            $('.modal-content').delay(2000).fadeIn().append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đầy đủ thông tin</b></p>');
                            $('.error-msg').delay(4000).fadeOut();
                        } else {
                            $('.modal-content').append('<p class="text-center error-msg" style="color:red;"><b>Vui lòng điền đầy đủ thông tin</b></p>');
                            $('.error-msg').delay(4000).fadeOut();
                        }

                    }

                });
                <?php if ($this->uri->segment(1) == 'danh-muc-san-pham' || $this->uri->segment(1) == 'tin-tuc' || $this->uri->segment(1) == 'thu-vien') : ?>
                    $('body').on('click', ".share-fb", function() {
                        <?php if ($this->uri->segment(1) == 'danh-muc-san-pham') : ?>
                            var link = $(this).closest('figure').find('.product-wrap-link').attr('href');
                        <?php elseif ($this->uri->segment(1) == 'tin-tuc') : ?>
                            var link = $(this).siblings('.news-overview-read-more').attr('href');
                        <?php elseif ($this->uri->segment(1) == 'thu-vien') : ?>
                            var link = $(this).siblings('a').attr('href');
                        <?php elseif ($this->uri->segment(1) == 'san-pham') : ?>
                            var link = <?php echo current_url(); ?>
                        <?php endif; ?>
                        link = "https://www.facebook.com/sharer/sharer.php?u=" + link;
                        var fbpopup = window.open(link, "pop", "width=600, height=400, scrollbars=no");
                        return false;
                    });

                <?php endif; ?>

                <?php if ($this->uri->segment(1) == 'doi-tac' || $this->uri->segment(1) == 'san-pham') : ?>
                    $('body').on('click', '.shops-wrap', function() {
                        $(this).parent('.shops-list').find(".active").removeClass('active');
                        var map_src = $(this).find('.map_src').val();
                        if (map_src.length < 10) {
                            var iframe = "<p class='google-map-iframe'>không có dữ liệu</p>";
                        } else {
                            var iframe = '<iframe class="google-map-iframe" src="https://' + map_src + '" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>'
                        }
                        $(this).parents('.tab-pane').find(".google-map-iframe").remove();
                        $(this).parents('.tab-pane').find(".google-map-iframe-wrap").append(iframe);
                        $(this).addClass('active');
                    });
                <?php endif; ?>
            });
            <?php if ($this->uri->segment(1) == 'san-pham') : ?>

                function share_fb_function() {
                    var link = window.location.href;
                    link = "https://www.facebook.com/sharer/sharer.php?u=" + link;
                    var fbpopup = window.open(link, "pop", "width=600, height=400, scrollbars=no");
                }
            <?php endif; ?>
        </script>
</body>

</html>