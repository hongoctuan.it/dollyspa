<!DOCTYPE html>
<html lang="vi">
<head>
    <link rel="shortcut icon" type="ico" href="<?php echo site_url('statics/default/img/logo_small.png')?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">     
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="vi">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Dolly Spa Quận Tân Phú - Dolly Spa Quan Tan Phu">
    <meta name="msapplication-tap-highlight" content="no">
    <title>Dolly spa quan tan phu</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>statics/default/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>statics/default/fontawesome-free/css/all.min.css">
    <?php
    switch ($this->uri->segment(1)) {
        case 'danh-muc-san-pham':
            $this->load->view('cms/_layout/datetime');
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/category.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/index-v2.css") . '">';
            break;
        case 'khuyen-mai':
            $this->load->view('cms/_layout/datetime');
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/gallery.css") . '">';
            break;
        case 'dat-cho':
            $this->load->view('cms/_layout/datetime');
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/booking.css") . '">';
            break;
        default:
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/about.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/index-v2.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/partner.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/product.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/gallery.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/fancybox-master/dist/jquery.fancybox.min.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/news.css") . '"><link rel="stylesheet" href="' . site_url("statics/default/css/news_detail.css") . '">';
    }
    ?>
    <link rel="stylesheet" href="<?php echo site_url('statics/default/css/header.css'); ?>">
    <?php if ($this->uri->segment(1) != "") : ?>
        <link rel="stylesheet" href="<?php echo site_url('statics/default/swiper/dist/css/swiper.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo site_url('statics/default/perfect-scrollbar/css/perfect-scrollbar.css'); ?>">
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.theme.default.min.css'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Yeseva+One&display=swap&subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese&display=swap" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'SourceSansPro';
            src: url(<?php echo base_url("statics/default/font/source_sans_pro/SourceSansPro-Light.ttf");?>);
            font-weight: 300;
        }
        @font-face {
            font-family: 'SourceSansPro';
            src: url(<?php echo base_url("statics/default/font/source_sans_pro/SourceSansPro-Regular.ttf");?>);
            font-weight: 400;
        }
        @font-face {
            font-family: 'SourceSansPro';
            src: url(<?php echo base_url("statics/default/font/source_sans_pro/SourceSansPro-SemiBold.ttf");?>);
            font-weight: 600;
        }
        @font-face {
            font-family: 'SourceSansPro';
            src: url(<?php echo base_url("statics/default/font/source_sans_pro/SourceSansPro-Bold.ttf");?>);
            font-weight: 700;
        }
        body {
            font-family: 'SourceSansPro';
        }
    </style>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Pattaya&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital@1&display=swap" rel="stylesheet">
</head>
<script src="https://kit.fontawesome.com/05a5e02e50.js" crossorigin="anonymous"></script>
<body>
<!-- Messenger Plugin chat Code -->
<div id="fb-root"></div>

<?php include 'facebook.php' ?>

<?php include 'google.php' ?>
<?php include 'zalo.php' ?>
<?php include 'order.php' ?>
    <header>
        <nav class="navbar-expand-lg navbar-dark navbar-lg">
            <div class="header-container navbar-desktop brand-bar">
                <a href="<?php echo site_url();?>"><div class="logo">
                    <img src="<?php echo site_url(''); ?>statics/default/img/logo.png" class="img-fluid" alt='logo' style="max-width:168px">
                </div>
                </a>
                <div class="icon_text">
                    <div class="col-md-4 address"><i class="fas fa-map-marker-alt"></i><?php echo $company_info['address']->value ?></div>
                    <div class="col-md-4 opentime"><i class="fa fa-clock-o" aria-hidden="true"></i> Th2 - CN, 9.30 - 22.00 Chỉ trừ Tết âm lịch</div>
                </div>
            </div>
        </nav>
        <nav class="navbar-expand-lg navbar-dark navbar-lg" id="navbartop">
            <div class="container menu-bar">
                <ul class="navbar-nav navbar-nav-lg">
                    <li class="nav-item <?php echo $this->uri->segment(1) == '' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text" href="<?php echo site_url(); ?>">Trang Chủ
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'danh-muc-san-pham' || $this->uri->segment(1) == 'san-pham' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text" href="<?php echo site_url(); ?>danh-muc-san-pham">Sản Phẩm</a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'dat-cho' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text" href="<?php echo site_url(); ?>dat-cho">Đặt Chỗ</a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'khuyen-mai' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text" href="<?php echo site_url(); ?>khuyen-mai">Khuyến Mãi</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-mobile" style="background-color:white">
            <div class="container">
                <div class='clearfix text-center' style="width:100%">
                    <a class="navbar-brand brand-sm text-white" href="<?php echo site_url(); ?>" style="color:white !important;">
                        <img src="<?php echo site_url(); ?>statics/default/img/logo.png" class="img-fluid" alt='logo' style="max-height:180px">
                    </a>
                </div>
                <div class="icon_text">
                    <div class="col-md-4 address"><i class="fas fa-map-marker-alt"></i><?php echo $company_info['address']->value?></div>
                    <div class="col-md-12 opentime"><i class="fa fa-clock-o" aria-hidden="true"></i> Th2 - CN, 9.30 - 22.00 Chỉ trừ Tết âm lịch</div>
                </div>
                <button class="navbar-toggler bg-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars burger_icon" style="color:#65B334"></i>
                </button>
                <div class="collapse navbar-collapse text-primary" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item <?php echo $this->uri->segment(1) == '' ? "active" : ""; ?>">
                            <a class="nav-link" href="<?php echo site_url(); ?>">Trang Chủ
                            </a>
                        </li>
                        <li class="nav-item <?php echo $this->uri->segment(1) == 'danh-muc-san-pham' || $this->uri->segment(1) == 'san-pham' ? "active" : ""; ?>">
                            <a class="nav-link" href="<?php echo site_url(); ?>danh-muc-san-pham">Sản Phẩm</a>
                        </li>
                        <li class="nav-item <?php echo $this->uri->segment(1) == 'dat-cho' ? "active" : ""; ?>">
                            <a class="nav-link" href="<?php echo site_url(); ?>dat-cho">Đặt Chỗ</a>
                        </li>
                        <li class="nav-item <?php echo $this->uri->segment(1) == 'khuyen-main' ? "active" : ""; ?>">
                            <a class="nav-link" href="<?php echo site_url(); ?>khuyen-mai">Khuyến Mãi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
