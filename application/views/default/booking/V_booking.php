<?php $action = site_url('default/booking/save');?>
<div class="banner">
    <h3>DỊCH VỤ</h3>
</div>
<section>
    <div class="row">
        <div class="col-md-12 content">
        <?php if(isset($_SESSION['system_msg'])){
					echo $_SESSION['system_msg'];unset($_SESSION['system_msg']);
				}?>
            <?php echo form_open_multipart($action);?>
							<div class="col-md-12 col-lg-6 content_item">
								<div class="form-group required">
									<label class="control-label title">Họ Tên</label>
									<input type="text" class="form-control" name="name" id="name" required />
								</div>
							</div>
							<div class="col-md-12 col-lg-6 content_item">
								<div class="form-group required">
									<label class="control-label title">Điện Thoại</label>
									<input type="text" class="form-control" name="phone" id="phone" required />
								</div>
							</div>
							<div class="col-md-12 col-lg-6 content_item">
								<div class="form-group required">
									<label class="control-label title">Ngày Hẹn</label>
									<input type="text" class="form-control" name="date" id="datepicker" required />
								</div>
							</div>
							<div class="col-md-12 col-lg-6 content_item">
								<div class="form-group required">
									<label class="control-label title">Giờ Hẹn</label>
									<input type="text" class="form-control" name="time" id="time" required />
								</div>
							</div>
                            <div class="col-md-12 col-lg-6 content_item">
								<div class="form-group required">
									<label class="control-label title">Số Người</label>
									<input type="text" class="form-control" name="number" id="number" required />
								</div>
							</div>
                            <div class="col-md-12 col-lg-6 content_item">
								<div class="form-group required title">
                                    <label for="inputState">Gói dịch vụ</label>
                                    <select id="product" name="product" class="form-control">
										<?php foreach($products as $product):?>
											<option value="<?php echo $product?$product->id:"";?>">
												<?php echo $product?$product->name:"";?>
											</option>
										<?php endforeach; ?>
                                    </select>
                                </div>
							</div>
                            <div class="col-md-12 content_item">
								<div class="form-group required">
									<label class="control-label title">Yêu Cầu Khác</label>
									<textarea type="text" class="form-control" name="note" id="note" ></textarea>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12" style="text-align:center">
								<button type="submit" id="formSubmit" class="btn btn-primary title">Gửi Đi</button>
							</div>
			<?php echo form_close(); ?>
        </div>
    </div>
</section>
