<?php 
class M_analytic extends CI_model
{
	//lay danh sach tat ca product
	public function getPaid()
	{
		$this->db->select_sum('paid');
		$query = $this->db->get("order");
		return $query->row()->paid;
	}
	public function getDebt()
	{
		$this->db->select_sum('debt');
		$query = $this->db->get("order");
		return $query->row()->debt;
	}

	public function getMonthByMonthCustomer(){
		$arr=array();
		for($i = 1; $i<=12; $i++){
			$arr[] = $this->getCustomerOfMonth($i);
		}
		return $arr;
	}

	public function getTotalIncomeCustomer(){
		$this->db->where('active',1);
		return $this->db->count_all_results('order');
	}

	public function getTotalExpireCustomer(){
		$maxTime = time();
		$minTime = date('c', strtotime('-30 days'));
		$minTime = new DateTime($minTime);
		$minTime = $minTime->getTimestamp();
		$this->db->where('active',0);
		$this->db->where('active_time >',$minTime);
		$this->db->where('active_time <=',$maxTime);
		return $this->db->count_all_results('order');
	}

	public function getExpireCustomer(){
		$maxTime = time();
		$minTime = date('c', strtotime('-30 days'));
		$minTime = new DateTime($minTime);
		$minTime = $minTime->getTimestamp();
		$arr=array();
		$this->db->select('c.name as customerName, c.phone, p.name as productName, o.active_time as expireTime');
		$this->db->from('order o');
		$this->db->join('customer c', 'o.customer_id = c.id');
		$this->db->join('product p', 'o.product_id = p.id');
		$this->db->order_by("c.name", "asc");
		$this->db->where('o.active',0);
		$this->db->where('o.active_time >',$minTime);
		$this->db->where('o.active_time <=',$maxTime);
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr; 
	}

	public function getIncomeMonthByMonth(){
		// $arr=array();
		$month = date('m');
		$year = date("Y");
		$temp = new stdClass();
		$temp->debt = $this->getIncomeOfMonth($month,"debt",$year)->debt;
		$temp->paid = $this->getIncomeOfMonth($month,"paid",$year)->paid;
		if(empty($temp->debt))
				$temp->debt=0;
		if(empty($temp->debt))
			$temp->paid=0;
		$arr[1] = $temp;
		$temp = new stdClass();
		if($month==5){
			$month=12;
			$year = date("Y")-1;
			$temp->debt = $this->getIncomeOfMonth($month,"debt",$year)->debt;
			$temp->paid = $this->getIncomeOfMonth($month,"paid",$year)->paid;
			if(empty($temp->debt))
				$temp->debt=0;
			if(empty($temp->debt))
				$temp->paid=0;
			$arr[0]=$temp;
		}else{
			$month=$month-1;
			$temp->debt = $this->getIncomeOfMonth($month,"debt",$year)->debt;
			$temp->paid = $this->getIncomeOfMonth($month,"paid",$year)->paid;
			if(empty($temp->debt))
				$temp->debt=0;
			if(empty($temp->debt))
				$temp->paid=0;
			$arr[0]=$temp;
		}
		return $arr;
	}

	private function getCustomerOfMonth($mount){
		$year = date("Y");
		$start_date=0;
		$end_date=0;
		$start;
		$end;
		switch ($mount) {
			case 1:
				$start = $year."-01-01";
				$end = $year."-02-1";
				break;
			case 2:
				$start = $year."-02-01";
				$end = $year."-03-01";
				break;
			case 3:
				$start = $year."-03-01";
				$end = $year."-04-01";
				break;
			case 4:
				$start = $year."-04-01";
				$end = $year."-05-01";
				break;
			case 5:
				$start = $year."-05-01";
				$end = $year."-06-01";
				break;
			case 6:
				$start = $year."-06-01";
				$end = $year."-07-01";
				break;
			case 7:
				$start = $year."-07-01";
				$end = $year."-08-01";
				break;
			case 8:
				$start = $year."-08-01";
				$end = $year."-09-01";
				break;
			case 9:
				$start = $year."-09-01";
				$end = $year."-10-01";
				break;
			case 10:
				$start = $year."-10-01";
				$end = $year."-11-01";
				break;
			case 11:
				$start = $year."-11-01";
				$end = $year."-12-01";
				break;
			case 12:
				$start = $year."-12-01";
				$end = ($year+1)."-01-01";
				break;
		}
		$start_date = new DateTime($start);
		$start_date = $start_date->getTimestamp();
		$end_date = new DateTime($end);	
		$end_date = $end_date->getTimestamp();
		$this->db->where('create_date >=',$start_date);
		$this->db->where('create_date <',$end_date);
		$query = $this->db->count_all_results('order');
		return $query;
	}

	public function getCustomerInfoOfMonth(){
		$month = date('m');
		$year = date("Y");
		$start_date=0;
		$end_date=0;
		$start;
		$end;
		switch ($month) {
			case 1:
				$start = $year."-01-01";
				$end = $year."-02-1";
				break;
			case 2:
				$start = $year."-02-01";
				$end = $year."-03-01";
				break;
			case 3:
				$start = $year."-03-01";
				$end = $year."-04-01";
				break;
			case 4:
				$start = $year."-04-01";
				$end = $year."-05-01";
				break;
			case 5:
				$start = $year."-05-01";
				$end = $year."-06-01";
				break;
			case 6:
				$start = $year."-06-01";
				$end = $year."-07-01";
				break;
			case 7:
				$start = $year."-07-01";
				$end = $year."-08-01";
				break;
			case 8:
				$start = $year."-08-01";
				$end = $year."-09-01";
				break;
			case 9:
				$start = $year."-09-01";
				$end = $year."-10-01";
				break;
			case 10:
				$start = $year."-10-01";
				$end = $year."-11-01";
				break;
			case 11:
				$start = $year."-11-01";
				$end = $year."-12-01";
				break;
			case 12:
				$start = $year."-12-01";
				$end = ($year+1)."-01-01";
				break;
		}
		$start_date = new DateTime($start);
		$start_date = $start_date->getTimestamp();
		$end_date = new DateTime($end);	
		$end_date = $end_date->getTimestamp();
		$arr=array();
		$this->db->select('c.name as customerName, c.phone, p.name as productName, o.active_time as expireTime, o.paid, o.debt, o.times');
		$this->db->from('order o');
		$this->db->join('customer c', 'o.customer_id = c.id');
		$this->db->join('product p', 'o.product_id = p.id');
		$this->db->order_by("c.name", "asc");
		$this->db->where('o.create_date >=',$start_date);
		$this->db->where('o.create_date <',$end_date);
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr; 
	}

	private function getIncomeOfMonth($mount,$type,$year){
		
		$start_date=0;
		$end_date=0;
		$start;
		$end;
		switch ($mount) {
			case 1:
				$start = $year."-01-01";
				$end = $year."-02-1";
				break;
			case 2:
				$start = $year."-02-01";
				$end = $year."-03-01";
				break;
			case 3:
				$start = $year."-03-01";
				$end = $year."-04-01";
				break;
			case 4:
				$start = $year."-04-01";
				$end = $year."-05-01";
				break;
			case 5:
				$start = $year."-05-01";
				$end = $year."-06-01";
				break;
			case 6:
				$start = $year."-06-01";
				$end = $year."-07-01";
				break;
			case 7:
				$start = $year."-07-01";
				$end = $year."-08-01";
				break;
			case 8:
				$start = $year."-08-01";
				$end = $year."-09-01";
				break;
			case 9:
				$start = $year."-09-01";
				$end = $year."-10-01";
				break;
			case 10:
				$start = $year."-10-01";
				$end = $year."-11-01";
				break;
			case 11:
				$start = $year."-11-01";
				$end = $year."-12-01";
				break;
			case 12:
				$start = $year."-12-01";
				$end = ($year+1)."-01-01";
				break;
		}
		$start_date = new DateTime($start);
		$start_date = $start_date->getTimestamp();
		$end_date = new DateTime($end);	
		$end_date = $end_date->getTimestamp();
		$this->db->select_sum($type);
		$this->db->where('create_date >=',$start_date);
		$this->db->where('create_date <',$end_date);
		$query = $this->db->get("order");
		return $query->row();
	}
}
?>