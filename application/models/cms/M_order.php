<?php 
class M_order extends CI_model
{
	//lay danh sach tat ca product
	public function getOrder()
	{
		$arr=array();
		$this->db->select('c.name as name, c.phone as phone, o.create_date, o.id as id, o.active as active, o.paid, o.debt, o.note, p.name as product_name, c.id as customer_id, o.end_date');
		$this->db->from('customer c');
		$this->db->join('order o', 'o.customer_id = c.id');
		$this->db->join('product p', 'o.product_id = p.id');
		$this->db->order_by("c.name", "asc");
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function getCustomerOrderList()
	{
		$arr=array();
		$this->db->select('c.id as customer_id');
		$this->db->from('customer c');
		$this->db->join('order o', 'o.customer_id = c.id');
		$this->db->join('product p', 'o.product_id = p.id');
		$this->db->group_by('o.customer_id');
		$this->db->order_by("c.name", "asc");
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function getCustomerByPhone($phone)
	{
		$arr=array();
		$this->db->select('id,phone');
		$this->db->from('customer');
		$this->db->where('phones', $phone);
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function getCustomer($id)
	{
		$arr=array();
		$this->db->select('c.name as name, c.phone as phone, o.id as id, o.active as active, o.paid, o.debt, o.note, o.times, o.end_date');
		$this->db->from('customer c');
		$this->db->join('order o', 'o.customer_id = c.id');
		$this->db->where('c.id', $id);
		$this->db->order_by("c.name", "asc");
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function searchOrderByCompare($column,$value)
	{
		$arr=array();
		$this->db->select('c.name as name, c.phone, o.create_date, o.id as id, o.active as active, o.paid, o.debt, o.note, p.name as product_name, c.id as customer_id');
		$this->db->from('customer c');
		$this->db->join('order o', 'o.customer_id = c.id');
		$this->db->join('product p', 'o.product_id = p.id');
		$this->db->where($column, $value);
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function searchOrderLike($column,$value)
	{
		$arr=array();
		$this->db->select('c.name as name, c.phone, o.create_date, o.id as id, o.active as active, o.paid, o.debt, o.note, p.name as product_name, c.id as customer_id');
		$this->db->from('customer c');
		$this->db->join('order o', 'o.customer_id = c.id');
		$this->db->join('product p', 'o.product_id = p.id');
		$this->db->like($column, $value);
		$this->db->order_by("c.name", "asc");
		
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function getMaxId()
    {
        $this->db->select_max('id');
        return $this->db->get('customer')->row_array();
    }

	public function getOrderPackage($id)
	{
		$arr=array();
		$this->db->select('c.name as name, c.phone as phone, o.id as id, o.active as active, o.paid, o.debt,  o.note, o.times, o.end_date');
		$this->db->from('customer c');
		$this->db->join('order o', 'o.customer_id = c.id');
		$this->db->where('o.id', $id);
		$this->db->order_by("c.name", "asc");
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function getOrderList($id)
	{
		$arr=array();
		$this->db->select('o.id as id, p.name, o.create_date, o.active as active, o.paid, o.debt,  o.note, o.times, o.end_date');
		$this->db->from('customer c');
		$this->db->join('order o', 'o.customer_id = c.id');
		$this->db->join('product p', 'o.product_id = p.id');
		$this->db->where('o.customer_id', $id);
		$this->db->order_by("c.name", "asc");
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function getOrderPackageDetail($id)
	{
		$arr=array();
		$this->db->select('od.date, od.user, od.note');
		$this->db->from('order o');
		$this->db->join('order_detail od', 'o.id = od.order_id');
		$this->db->where('od.order_id', $id);
		$this->db->order_by("o.id", "asc");
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

}
?>