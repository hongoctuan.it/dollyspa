window.onscroll = function() {scrollFunction()};
function scrollFunction() 
{
    if (document.documentElement.scrollTop > 100) 
    {
        document.getElementById("navbartop").style.padding = "0px";
        document.getElementById("navbartop").style.width = "95%";
        document.getElementById("navbartop").style.height = "75px";
        document.getElementById("navbartop").style.margin = "0 auto";
        document.getElementById("navbartop").style.marginTop = "10px";
        document.getElementById("navbartop").style.backgroundColor = "#008000";
        document.getElementById("navbartop").style.boxShadow = "-1px 1px 5px";
        $(".search-form").css("max-height", "48px");
        $(".brand-logo").css("margin-top","0px");
        $(".brand-slogan").css("margin-top","8px");
        $(".navbar-lg").addClass("backGroundNav");
        $(".navbar-mobile").addClass("backGroundNav");
    } 
    else 
    {

        document.getElementById("navbartop").style.padding = "0px";
        document.getElementById("navbartop").style.width = "100%";
        document.getElementById("navbartop").style.height = "94px";
        document.getElementById("navbartop").style.margin = "0 auto";
        document.getElementById("navbartop").style.backgroundColor = "#008000dc";
        $(".search-form").css("max-height", "53px");
        $(".brand-logo").css("margin-top","5px");
        $(".brand-slogan").css("margin-top","13px");
          $(".navbar-desktop").removeClass("hideNav");
          $(".navbar-lg").removeClass("backGroundNav");
          $(".navbar-mobile").removeClass("backGroundNav");
    } 
    if (document.documentElement.scrollTop > 400) 
    {
        document.getElementById("back-to-top-btn").style.transform = "translateX(-20px)";
    } 
    else 
    {
          document.getElementById("back-to-top-btn").style.transform = "translateX(80px)";
    } 
}

$(document).ready(function(){
    $('.grid').isotope({
        // set itemSelector so .grid-sizer is not used in layout
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
        // use element for option
        columnWidth: '.grid-sizer'
        }
    })
    $('.grid').imagesLoaded().progress(function () { 
        $('.grid').masonry() 
    });

    $("#back-to-top-btn").on("click", function (e) {
        e.preventDefault();
        $("html,body").animate({
            scrollTop: 0
        }, 700);
    });
    $(".home-banner-slide-carousel.owl-carousel").owlCarousel({
        loop:true,
        margin:15,
        nav:true,
        navText:["<i class='fas fa-caret-left'></i>","<i class='fas fa-caret-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
    $(".owl-carousel.featured-products-slide").owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText:["<i class='fas fa-caret-left'></i>","<i class='fas fa-caret-right'></i>"],
        item:4,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });


    $(".category-banner-title").mouseenter(function(){
        var alternate = anime({
            targets: ".product-back-icon",
            translateX: -15,
            direction: "alternate",
            duration: 800
            });
    });
    $(".category-banner-title").mouseleave(function(){
        var alternate = anime({
            targets: ".product-back-icon",
            translateX: 0,
            });
    });
    var alternate = anime({
        targets: ".product-back-icon",
        translateX: -15,
        direction: "alternate",
        duration: 600
      });
    if($(window).width() <= 991)
    {
        $(".dropdown-btn").click(function(){
            $(".category-list").toggleClass("show-category-list");
        });
        $(".category-list").click(function(){
            $(".category-list").toggleClass("show-category-list");
        });
    }
    $(window).resize(function () {
        if($(window).width() <= 991)
        {
            $(".dropdown-btn").click(function(){
                $(".category-list").toggleClass("show-category-list");
            });
            $(".category-list").click(function(){
                $(".category-list").toggleClass("show-category-list");
            });
        }
    });
});

// var galleryThumbs = new Swiper(".gallery-thumbs", {
//     spaceBetween: 1,
//     slidesPerView: 3,
//     loopedSlides: 3,
//     watchSlidesVisibility: true,
//     watchSlidesProgress: true,
// });

// var galleryTop = new Swiper(".gallery-top", {
// spaceBetween: 10,
// loop:true,
// loopedSlides: 5,
// navigation: {
//     nextEl: ".swiper-button-next",
//     prevEl: ".swiper-button-prev",
// },
// thumbs: {
//     swiper: galleryThumbs,
// },
// });
var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 4,
    loop: true,
    freeMode: true,
    loopedSlides: 5, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    loop:true,
    loopedSlides: 5, //looped slides should be the same
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    thumbs: {
      swiper: galleryThumbs,
    },
  });

var featuredProducts = new Swiper(".featured-shops-slide", {
    spaceBetween: 10,
    slidesPerView: 6,
    loop: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      breakpoints: {
        991: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        575: {
            slidesPerView:2,
            spaceBetween: 30,
        }
    }
    });

  const ps = new PerfectScrollbar("#perfect-scrollbar-product-info", {
    wheelSpeed: 1,
    wheelPropagation: true,
    minScrollbarLength: 20
  });



