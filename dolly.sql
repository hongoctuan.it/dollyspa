-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 20, 2021 at 06:51 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dolly`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `slogan` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `slogan`, `short_des`, `detail_des`, `slug`, `img`) VALUES
(1, '1', 'Thành lập từ năm 2010, Cát Mộc Spa ẩn mình tại Quận 1, Tp. HCM và mang hơi thở của kiến trúc Việt Nam đương đại kết hợp hài hòa với thiên nhiên và hương thơm tinh dầu... [Xem thêm]', '323', 'gioi_thieu', '1557255425_123.png');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `customer_id`, `date`, `time`, `number`, `product`, `note`, `active`) VALUES
(1, 0, '1', '1', 1, 0, '1', 0),
(2, 0, '1', '1', 1, 0, '1', 0),
(3, 0, '1', '1', 1, 0, '1', 0),
(4, 0, '1', '1', 1, 0, '1', 0),
(5, 0, '1', '1', 1, 0, '1', 0),
(6, 9, '1', '1', 1, 0, '1', 0),
(7, 10, '1', '1', 1, 0, '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `level` int(11) NOT NULL,
  `img_large` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `img`, `parent`, `slug`, `active`, `deleted`, `level`, `img_large`) VALUES
(1, 'CHAM SOC DA', '', NULL, 0, 'hoa-pham', 1, 0, 0, '1558095411_img_content_header_home_use.jpg'),
(2, 'SUC KHOE', NULL, NULL, 0, '', 1, 0, 0, ''),
(3, 'Tin Khuyến Mãi', '<p>mo ta</p>\r\n', NULL, 0, 'gia_dung', 1, 0, 0, ''),
(4, 'GOI DICH VU', NULL, NULL, 0, '', 1, 0, 0, ''),
(15, 'cham soc', 'cham soc mo ta', NULL, 0, '', 1, 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_updated` datetime NOT NULL,
  `previous_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `info`, `value`, `last_updated`, `previous_value`) VALUES
(1, 'address', 'Quận Thủ Đức TP HCM, Việt Nam', '2019-05-18 14:15:08', NULL),
(2, 'phone', '123456789', '2019-05-18 14:15:08', NULL),
(3, 'facebook', '#', '2019-05-18 14:15:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `phone`, `active`) VALUES
(1, '1', '1', 0),
(2, '1', '1', 0),
(3, '1', '1', 0),
(4, '1', '1', 0),
(5, '1', '1', 0),
(6, '1', '1', 0),
(7, '1', '1', 0),
(8, '1', '1', 0),
(9, '1', '1', 0),
(10, '1', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `detaildistribution`
--

CREATE TABLE `detaildistribution` (
  `id` int(11) NOT NULL,
  `distributionId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL,
  `paymentProduct` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distribution`
--

CREATE TABLE `distribution` (
  `id` int(11) NOT NULL,
  `distributorId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `totalPayment` int(11) NOT NULL DEFAULT 0,
  `userId` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE `distributor` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_detail`
--

CREATE TABLE `gallery_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` text COLLATE utf8_unicode_ci NOT NULL,
  `end_date` int(11) NOT NULL,
  `discount` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallery_detail`
--

INSERT INTO `gallery_detail` (`id`, `name`, `description`, `img`, `gallery_id`, `active`, `slug`, `start_date`, `end_date`, `discount`, `deleted`) VALUES
(20, '1', '1Đây là hình 11', '1621732404_login_background.jpeg', 3, 1, '1', '1', 1, '2', 0),
(29, '2', '2', '1621733530_login_background.jpeg', 0, 1, '2', '2', 2, '2', 0),
(30, '3', '3', '1621733547_login_background.jpeg', 0, 1, '3', '3', 3, '3', 0),
(31, '4', '4', '', 0, 1, '4', '4', 4, '4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `time` text COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `title`, `time`, `des`, `img`, `deleted`) VALUES
(1, '15', '23', '34', '1557184038_123.png', 0),
(2, '1', '2', '2', '1557184176_screen_shot_2019-03-24_at_23.01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_banner`
--

CREATE TABLE `home_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) DEFAULT 1,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_banner`
--

INSERT INTO `home_banner` (`id`, `name`, `promotion_id`, `img`, `active`, `deleted`, `link`, `description`) VALUES
(1, 'Banner 1', 1, '1556574471_home-banner-slide.jpg', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_hot_product`
--

CREATE TABLE `home_hot_product` (
  `id` int(11) NOT NULL,
  `img` int(11) NOT NULL,
  `des_full` text COLLATE utf8_unicode_ci NOT NULL,
  `des_01` text COLLATE utf8_unicode_ci NOT NULL,
  `des_02` text COLLATE utf8_unicode_ci NOT NULL,
  `des_03` text COLLATE utf8_unicode_ci NOT NULL,
  `des_04` text COLLATE utf8_unicode_ci NOT NULL,
  `des_05` text COLLATE utf8_unicode_ci NOT NULL,
  `des_06` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_hot_product`
--

INSERT INTO `home_hot_product` (`id`, `img`, `des_full`, `des_01`, `des_02`, `des_03`, `des_04`, `des_05`, `des_06`) VALUES
(1, 1558446179, '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n', '<p>1</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `home_info_compa`
--

CREATE TABLE `home_info_compa` (
  `id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `time` text COLLATE utf8_unicode_ci NOT NULL,
  `map` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_info_compa`
--

INSERT INTO `home_info_compa` (`id`, `address`, `phone`, `email`, `time`, `map`) VALUES
(1, '2', '1', '23', '<ul class=\"stm_schedule_list\"><li class=\"schedule-list_item\"><span class=\"schedule_day\">Th2 - CN:</span><span class=\"schedule_time\">9.30 - 22.00</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\">Lễ - Tết:</span><span class=\"schedule_time\">Mở cửa</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\">Tết Âm lịch:</span><span class=\"schedule_time\">Đóng cửa</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\">Chốt hẹn:</span><span class=\"schedule_time\">8.30 pm</span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li><li class=\"schedule-list_item\"><span class=\"schedule_day\"></span><span class=\"schedule_time\"></span></li></ul>', 'map.png');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `parent` int(11) NOT NULL DEFAULT 0,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `description`, `img`, `active`, `parent`, `slug`, `deleted`, `created_at`, `created_by`) VALUES
(1, 'Tin Tức Chung', 'bn', '1556565617_1556022579_5_tay_dang_kem_2.JPG', 1, 0, 'tin-tuc-chung', 0, '2019-05-22 07:07:06', NULL),
(2, 'Tin Cty', 'a', '1556565782_1556023560_12_giay_uot_lau_mat_1.jpg', 1, 0, 'tin-cty', 1, '2019-05-22 07:07:06', NULL),
(3, 'Tin Khuyến Mãi', '<p>mo ta</p>\r\n', '1621728847_login_background.jpeg', 1, 0, 'tin-khuyen-mai', 0, '2019-05-22 07:07:06', NULL),
(4, 'Tin Tức 1', 'Đây là tin tức 1', '1556021072_1_2_3_4_dd_tay_da_nang_huong_trai_cay_1.JPG', 1, 3, 'tin-tuc-1', 1, '2019-05-22 07:07:06', NULL),
(5, 'Tin Tức 2', 'Đây là tin tức 2', '1556565782_1556023560_12_giay_uot_lau_mat_1.jpg', 1, 1, 'tin_tuc_2', 0, '2019-05-22 07:07:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `area` int(50) NOT NULL,
  `latitude` int(11) NOT NULL DEFAULT 0,
  `longitude` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 0,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`id`, `name`, `address`, `phone`, `area`, `latitude`, `longitude`, `active`, `slug`, `deleted`) VALUES
(1, 'cua hang 1', 'dong nai', '1234', 1, 1, 2, 0, '', 0),
(2, '1', '1', '1', 0, 1, 1, 1, 'cua_hang', 0);

-- --------------------------------------------------------

--
-- Table structure for table `partner_registry`
--

CREATE TABLE `partner_registry` (
  `id` int(11) NOT NULL,
  `representator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `producer`
--

CREATE TABLE `producer` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `producerId` int(11) DEFAULT NULL,
  `img3` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img2` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img1` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `price` int(20) NOT NULL DEFAULT 0,
  `promotion_id` int(11) DEFAULT NULL,
  `inStock` int(11) NOT NULL DEFAULT 0,
  `featured` tinyint(4) NOT NULL DEFAULT 0,
  `hot` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1,
  `img4` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `short_des`, `detail_des`, `category_id`, `producerId`, `img3`, `img2`, `img1`, `event_id`, `price`, `promotion_id`, `inStock`, `featured`, `hot`, `time`, `active`, `img4`, `slug`, `deleted`) VALUES
(1, 'Khăn Vải Không dệt làm sạch phòng ngủ 3', 'Làm sạch và diệt khuẩn phòng ngủ ', '<p>L&agrave;m sạch v&agrave; diệt khuẩn ph&ograve;ng ngủ. Sự điện ph&acirc;n nước được sử dụng trong sản phẩm n&agrave;y được cấu th&agrave;nh từ sự ph&acirc;n giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm l&agrave;m ph&acirc;n giải c&aacute;c chất dơ tr&ecirc;n bề mặt đồ d&ugrave;ng. Ngo&agrave;i ra chất benzalkonium chloride l&agrave; chất c&oacute; t&iacute;nh khử độc v&agrave; diệt khuẩn được sử dụng l&agrave;m sạch m&ocirc;i trường. Quy c&aacute;ch : 48 g&oacute;i / th&ugrave;ng (1g&oacute;i = 20 miếng).</p>\r\n', 1, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 0, NULL, 0, 0, 1, 10, 1, NULL, 'khan-vai-khong-det-lam-sach-phong-ngu-3', 1),
(27, 'Khăn Vải Không dệt làm sạch phòng ngủ 1', 'Làm sạch và diệt khuẩn phòng ngủ ', '<p>L&agrave;m sạch v&agrave; diệt khuẩn ph&ograve;ng ngủ. Sự điện ph&acirc;n nước được sử dụng trong sản phẩm n&agrave;y được cấu th&agrave;nh từ sự ph&acirc;n giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm l&agrave;m ph&acirc;n giải c&aacute;c chất dơ tr&ecirc;n bề mặt đồ d&ugrave;ng. Ngo&agrave;i ra chất benzalkonium chloride l&agrave; chất c&oacute; t&iacute;nh khử độc v&agrave; diệt khuẩn được sử dụng l&agrave;m sạch m&ocirc;i trường. Quy c&aacute;ch : 48 g&oacute;i / th&ugrave;ng (1g&oacute;i = 20 miếng).</p>\r\n', 2, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 0, NULL, 0, 0, 1, 20, 1, NULL, 'khan-vai-khong-det-lam-sach-phong-ngu-1', 0),
(28, 'Khăn Vải Không dệt làm sạch phòng ngủ (20 miếng)', 'Làm sạch và diệt khuẩn phòng ngủ ', 'Làm sạch và diệt khuẩn phòng ngủ. Sự điện phân nước được sử dụng trong sản phẩm này được cấu thành từ sự phân giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm làm phân giải các chất dơ trên bề mặt đồ dùng. Ngoài ra chất benzalkonium chloride là chất có tính khử độc và diệt khuẩn được sử dụng làm sạch môi trường. Quy cách :  48 gói / thùng (1gói = 20 miếng).', 1, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 0, NULL, 0, 0, 1, 30, 1, NULL, '', 0),
(29, 'Khăn Vải Không dệt làm sạch phòng ngủ (20 miếng)', 'Làm sạch và diệt khuẩn phòng ngủ ', 'Làm sạch và diệt khuẩn phòng ngủ. Sự điện phân nước được sử dụng trong sản phẩm này được cấu thành từ sự phân giải điện dung dịch kali cacbonat ở mức phụ gia thực phẩm làm phân giải các chất dơ trên bề mặt đồ dùng. Ngoài ra chất benzalkonium chloride là chất có tính khử độc và diệt khuẩn được sử dụng làm sạch môi trường. Quy cách :  48 gói / thùng (1gói = 20 miếng).', 1, NULL, '', '', '1556565765_1556023453_product4.jpg', NULL, 0, NULL, 0, 0, 1, 40, 1, NULL, '', 0),
(32, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(33, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(34, '2', '2', '<p>3</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 2, NULL, 0, 0, 0, 0, 1, NULL, '2', 1),
(35, '2', '2', '<p>3</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 2, NULL, 0, 0, 0, 0, 1, NULL, '2', 1),
(36, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, '', NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(37, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, '1621655457_login_background.jpeg', NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 0),
(38, '3', '3', '<p>3</p>\r\n', 1, NULL, NULL, NULL, '1621655793_hinh-phoi-canh-giai-doan-2-du-an-the-metropole-thu-thiem-sonkimland.jpg', NULL, 3, NULL, 0, 0, 0, 0, 1, NULL, '3', 0),
(39, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(40, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(41, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(42, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(43, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(44, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(45, '2', '2', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 2, NULL, 0, 0, 0, 0, 1, NULL, '2', 1),
(46, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1),
(47, '1', '1', '<p>1</p>\r\n', 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, 0, 0, 1, NULL, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `starDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `discount` float NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `gift_productId` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `name`, `description`, `starDate`, `endDate`, `discount`, `type`, `img`, `gift_productId`, `deleted`) VALUES
(1, 'asd', 'asdas', '2019-04-23 00:00:00', '2019-04-30 00:00:00', 1235, 0, 'promotion.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `repcomment`
--

CREATE TABLE `repcomment` (
  `id` int(11) NOT NULL,
  `commentId` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SEO`
--

CREATE TABLE `SEO` (
  `id` int(11) NOT NULL,
  `page` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SEO`
--

INSERT INTO `SEO` (`id`, `page`, `meta`) VALUES
(1, 'Trang chủ', '123'),
(2, 'Sản phẩm', '1123'),
(3, 'Hình ảnh', '2'),
(4, 'Tin tức', '3'),
(5, 'Về chúng tôi', '4'),
(6, 'Đối tác', '5'),
(7, 'Danh mục', '7');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `full_name`, `email`, `phone`, `role`, `deleted`) VALUES
(2, 'admin', '3f644196140d4ba5779022ffda539c537db3b603', 'qưe', 'hongoctuan.it@gmail.com', '090909', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `category` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `category` ADD FULLTEXT KEY `name_2` (`name`,`description`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commentProduct` (`productId`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detaildistributionDitributon` (`distributionId`),
  ADD KEY `fk_detaildistributionProduct` (`productId`);

--
-- Indexes for table `distribution`
--
ALTER TABLE `distribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_distributionDistributor` (`distributorId`),
  ADD KEY `fk_distributionUser` (`userId`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banner`
--
ALTER TABLE `home_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `news` ADD FULLTEXT KEY `name` (`name`,`description`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name` (`name`,`address`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name_2` (`name`,`address`);
ALTER TABLE `partner` ADD FULLTEXT KEY `name_3` (`name`,`address`);

--
-- Indexes for table `partner_registry`
--
ALTER TABLE `partner_registry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producer`
--
ALTER TABLE `producer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producer` (`producerId`),
  ADD KEY `fk_category` (`category_id`),
  ADD KEY `fk_event` (`event_id`),
  ADD KEY `fk_promotion` (`promotion_id`);
ALTER TABLE `product` ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `product` ADD FULLTEXT KEY `name_2` (`name`,`detail_des`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repcomment`
--
ALTER TABLE `repcomment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_repcommentComment` (`commentId`);

--
-- Indexes for table `SEO`
--
ALTER TABLE `SEO`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distribution`
--
ALTER TABLE `distribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distributor`
--
ALTER TABLE `distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `home_banner`
--
ALTER TABLE `home_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partner_registry`
--
ALTER TABLE `partner_registry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `producer`
--
ALTER TABLE `producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `repcomment`
--
ALTER TABLE `repcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `SEO`
--
ALTER TABLE `SEO`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_commentProduct` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Constraints for table `detaildistribution`
--
ALTER TABLE `detaildistribution`
  ADD CONSTRAINT `fk_detaildistributionDitributon` FOREIGN KEY (`distributionId`) REFERENCES `distribution` (`id`),
  ADD CONSTRAINT `fk_detaildistributionProduct` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Constraints for table `distribution`
--
ALTER TABLE `distribution`
  ADD CONSTRAINT `fk_distributionDistributor` FOREIGN KEY (`distributorId`) REFERENCES `distributor` (`id`),
  ADD CONSTRAINT `fk_distributionUser` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `fk_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `fk_producer` FOREIGN KEY (`producerId`) REFERENCES `producer` (`id`),
  ADD CONSTRAINT `fk_promotion` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`);

--
-- Constraints for table `repcomment`
--
ALTER TABLE `repcomment`
  ADD CONSTRAINT `fk_repcommentComment` FOREIGN KEY (`commentId`) REFERENCES `comment` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
